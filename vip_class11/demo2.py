# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/12 20:33
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 关键字驱动

# robotframework==>keyword framework==> 主旨：低代码量（对于简单的项目，甚至是0代码）
# 所谓的关键字， 按照测试需求，将测试用例中所涉及的操作进行封装，也称为操作层的封装
# 关键字的本质：针对功能的封装，其目的就是为了简化代码


# 关联的本质：先存，后用
from vip_class11.webkeys import Web

web = Web()
web.relation_dict = {'a': 'value a', 'text': 'zxtest'}
web.save_params('a', 'b')
print(web.relation_dict)

# web.save_params('c', '{a}')
args = ['ccc', '{a}', 'asdw']
for key in web.relation_dict:
    for i in range(len(args)):
        args[i] = args[i].replace('{' + str(key) + '}', str(web.relation_dict.get(key)))
print(args)