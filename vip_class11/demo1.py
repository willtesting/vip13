# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/12 20:02
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 断言


# UI自动化断言
"""
1. 用于做流程性的验证：冒烟测试/验收测试  一般可以不用断言
2. 针对具体功能的断言：
    a. 文本断言：   预期值 == 实际值？ ==>  元素定位.text
    b. title断言：
    c. 通过显示等待断言：
    d. 最原始的方法： 通过 if...else语句自行判断   try...except

"""
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from vip_class9.example import Web

web = Web()
web.open_browser()

# 登录
web.get_url('http://testingedu.com.cn:8000/Home/user/login.html')
web.input('//*[@id="username"]', '2929184523@qq.com')
web.input('//*[@id="password"]', '123457')
web.input('//*[@id="verify_code"]', '1111')
web.click('//a[@name="sbtbutton"]')

# 断言
# user_status = web.driver.find_element(By.XPATH, '//a[text()="安全退出"]').text
# # print(user_status)
# assert "安全退出" == user_status

WebDriverWait(web, 5, 0.5).until(lambda el: web.driver.find_element('xpath', '//a[text()="安全退出"]'),
                                 message='登录失败...')

web.sleep(3)
web.quit()