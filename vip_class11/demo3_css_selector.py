# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/12 21:21
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述

# 案例：百度搜索框
# 1.通过id属性写
# 比如百度首页的搜索框 #kw
# 2. 通过class属性写 .s_ipt
# 3. 通过标签名写,直接写标签名即可。但是通常来说，一个页面相同的标签很多，一般需要配合上面的方法来补充比如：input#kw
# 4. 可以通过层级关系来写：格式为xxx > xxx 比如：div > form > span > input.s_ipt
# 层级关系还可以把 > 省略 用空格代替 span input.s_ipt