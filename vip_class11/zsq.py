# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/12 22:04
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述


# 关联装饰器
import random
import time


def relations(func):
    def warpper(*args, **kwargs):
        # 把元组参数转成列表
        args = list(args)
        self = args[0]
        # 获取毫秒的时间戳
        self.relation_dict['time'] = str(int(time.time() * 1000))
        # 获取随机数[0,1000]
        self.relation_dict['random'] = str(random.randint(0,1000))

        for key in self.relation_dict:
            # 遍历所有的键
            for i in range(1, len(args)):
                # 提升运行效率
                if str(args[i]).__contains__('{'):
                    # 把以{key}形式存在的字符串，替换为字典里的值
                    args[i] = str(args[i]).replace('{' + str(key) + '}', str(self.relation_dict.get(key)))

        # print(args)
        # 调用关键字
        res = func(*args, **kwargs)
        return res
    return warpper

# relations = {'orderid': '202020020221028901'}
# input('xxxx{}yyyy')