# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/14 20:03
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述

# 12306滑块
import time

from vip_class11.webkeys import Web

web = Web()
web.open_browser()

# web.get_url('https://kyfw.12306.cn/otn/resources/login.html')
# web.input('//*[@id="J-userName"]', '2929184523@qq.com')
# web.input('//*[@id="J-password"]', '123456')
# web.click('//*[@id="J-login"]')
# web.slide_12306()
# time.sleep(3)
# web.driver.quit()

web.get_url('http://testingedu.com.cn:8000/index.php/Home/user/login.html')
web.input('//*[@id="username"]', '2929184523@qq.com')
web.input('//*[@id="password"]', '123456')
ver = web.get_verfiy('//*[@id="verify_code_img"]')
web.input('//*[@id="verify_code"]', ver)
web.click('//*[@id="loginform"]/div/div[6]/a')

time.sleep(3)
web.driver.quit()