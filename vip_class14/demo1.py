# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/19 20:06
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：迭代器，生成器
"""

my_l = [1, 2, 3, 4]
# # 成员遍历
# for i in my_l:
#     print(i)

# 迭代器
my_iter = iter(my_l)
# print(my_iter)
# for i in my_iter:
#     print(i)

# 下一个
print(next(my_iter))

while True:
    try:
        n = my_iter.__next__()
        print(n)
    except:
        break


def my_generator():
    """生成器"""
    for i in range(1,5):
        # 在调用生成器运行的过程中，每次遇到 yield 时函数会暂停并保存当前所有的运行信息，
        # 返回 yield 的值, 并在下一次执行 next() 方法时从当前位置继续运行。
        print('生成前')
        yield i
        print('生成后')


# 调用生成器，得到一个迭代器
m = my_generator()
print(m)
print(m.__next__())
print(m.__next__())
