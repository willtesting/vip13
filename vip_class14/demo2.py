# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/19 20:52
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# 获取项目绝对路径
import os

path = os.path.abspath(__file__)
print(path)
proname = 'vip13'
# 通过项目名字，截取到绝对路径
path = path[:path.rfind(proname) + len(proname)]
print(path)

