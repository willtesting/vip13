# coding:utf8
from smtplib import SMTP_SSL
from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import yaml

from vip_class16.common.Logger import path, logger


class Mail:
    """
        powered by Mr Will
        at 2018-12-22
        用来获取配置并发送邮件
    """

    def __init__(self):
        # 读取配置文件
        f = open(file=path + "lib/conf.yml", mode='r', encoding="utf-8")
        self.mail_info = yaml.safe_load(stream=f)['mail']
        f.close()

        # 发件人
        self.mail_info['from'] = self.mail_info['username']
        # smtp服务器域名，截取域名，然后拼上smtp.
        self.mail_info['hostname'] = 'smtp.' + self.mail_info['username']\
        [self.mail_info['username'].rfind('@') + 1:self.mail_info['username'].__len__()]

        logger.info(self.mail_info)


    def send(self, text):
        # 这里使用SMTP_SSL就是默认使用465端口，如果发送失败，可以使用587
        smtp = SMTP_SSL(self.mail_info['hostname'])
        smtp.set_debuglevel(0)

        ''' SMTP 'ehlo' command.
        Hostname to send for this command defaults to the FQDN of the local
        host.
        '''
        smtp.ehlo(self.mail_info['hostname'])
        # 登录
        smtp.login(self.mail_info['username'], self.mail_info['password'])

        # 普通HTML邮件
        # msg = MIMEText(text, 'html', self.mail_info['mail_encoding'])

        # 支持附件的邮件
        msg = MIMEMultipart()
        msg.attach(MIMEText(text, 'html', self.mail_info['mail_encoding']))
        msg['Subject'] = Header(self.mail_info['mail_subject'], self.mail_info['mail_encoding'])
        # msg['from'] = self.mail_info['from']
        # 添加自定义昵称
        h = Header(self.mail_info['mailnick'], 'utf-8')
        h.append('<' + self.mail_info['from'] + '>', 'ascii')
        msg["from"] = h

        # logger.debug(self.mail_info)
        # logger.debug(text)
        msg['to'] = ','.join(self.mail_info['to'])
        msg['cc'] = ','.join(self.mail_info['cc'])
        receive = self.mail_info['to']
        receive += self.mail_info['cc']

        # 添加附件
        for i in range(len(self.mail_info['filepaths'])):
            att1 = MIMEText(open(self.mail_info['filepaths'][i], 'rb').read(), 'base64', 'utf-8')
            att1['Content-Type'] = 'application/octet-stream'
            # att1['Content-Disposition'] = 'attachment; filename= "'+self.mail_info['filenames'][i]+'"'
            att1.add_header('Content-Disposition', 'attachment', filename=('gbk', '', self.mail_info['filenames'][i]))

            msg.attach(att1)

        try:
            smtp.sendmail(self.mail_info['from'], receive, msg.as_string())
            smtp.quit()
            logger.info('邮件发送成功')
        except Exception as e:
            logger.error('邮件发送失败：')
            logger.exception(e)



if __name__ == '__main__':
    mail = Mail()
    # mail.mail_info['filepaths'] = [r'D:\PythonPro\gitpro\vip13\lib\result-电商项目用例.xlsx',r'D:\PythonPro\gitpro\vip13\lib\电商项目用例.xlsx']
    # mail.mail_info['filenames'] = ['result-HTTP接口用例-all.xlsx','电商项目用例.xlsx']
    f = open(path + 'lib/' + mail.mail_info.get('mailmodule'),'r',encoding='utf8')
    html = f.read()
    f.close()
    mail.send(html)