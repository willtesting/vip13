# coding:utf8
from vip_class16.common.Excel import Reader
from vip_class16.common.Logger import logger, path


class Result:
    """
    统计Excel用例执行结果信息
    powered by William
    2018-11-6
    copyright:testingedu.com.cn
    """

    def __init__(self):
        # 用于记录所有用例的统计数据
        self.sumarry = {}
        # 统计分组信息
        self.groups = []

    def get_res(self, result_path):
        # 用于记录所有用例的统计数据
        self.sumarry.clear()
        # 统计测试用例集的用例总条数
        totalcount = 0
        # 统计所有用例中通过用例的条数数
        totalpass = 0

        reader = Reader()
        reader.open_excel(result_path)
        lines = reader.readline()
        self.sumarry['runtype'] = lines[1][1]
        self.sumarry['title'] = lines[1][2]
        self.sumarry['starttime'] = lines[1][3]
        self.sumarry['endtime'] = lines[1][4]
        # 获取所有sheet页面
        for n in reader.get_sheets():
            # logger.info(n)
            # 从第一个页面开始解析
            reader.set_sheet(n)
            # 获取sheet的行数，用来遍历
            row = reader.rows

            # 遍历sheet里面所有用例
            lines = reader.readline()
            for i in range(1, row):
                line = lines[i]
                # logger.info(line)
                # 查找记录了分组信息的行
                # 如果第一列（分组信息）和第二列（类别或用例名）不同时为空,则不是用例，执行非用例的操作
                if not (line[0] == '' and line[1] == ''):
                    pass

                # 非用例行判断结束
                # 第一列信息和第二列信息均为空的行，即用例行，这时开始进行用例数、通过数、状态的统计。
                else:
                    # 判断执行结果列，如果为空，将flag置为false,视为该行有误，不纳入用例数量计算
                    if len(line) < 8 or line[7] == '':
                        pass
                    # 执行结果不为空，则将用例统计数自增
                    else:
                        totalcount = totalcount + 1
                        # logger.info(line)
                        # 如果通过，则通过数和总通过数均自增
                        if line[7] == "PASS":
                            totalpass += 1
            # for循环结束

        # 所有用例执行概况
        # logger.info(totalpass)
        # 计算通过率
        try:
            # 通过率，保留两位小数
            p = int(totalpass * 10000 / totalcount)
            passrate = p / 100
        except Exception as e:
            passrate = 0.0
            logger.exception(e)

        # 用例总数
        self.sumarry["casecount"] = str(totalcount)
        # 通过率
        self.sumarry["passrate"] = str(passrate)
        if totalcount == totalpass:
            self.sumarry['status'] = "PASS"
        else:
            self.sumarry['status'] = "FAIL"
        # logger.info(self.sumarry)
        return self.sumarry

    def get_groups(self, result_path):
        # 用于记录执行结果，逻辑为，只要分组中出现一个失败用例，则认为该分组执行失败，与flag联合使用。
        self.groups.clear()
        # 每一个分组统计信息为列表
        groupinfo = []

        # 统计每一个分组的用例总条数
        totalcount = 0
        # 统计分组用例中通过用例的条数数
        totalpass = 0

        reader = Reader()
        reader.open_excel(result_path)
        # 获取所有sheet页面
        for n in reader.get_sheets():
            # logger.info(n)
            # 从第一个页面开始解析
            reader.set_sheet(n)
            # 获取sheet的行数，用来遍历
            row = reader.rows

            # 标识一个分组是否统计完，用来区分是否是第一个分组
            gflag = True

            # 遍历sheet里面所有用例
            lines = reader.readline()

            for i in range(1, row):
                # 查找记录了分组信息的行
                # 如果第一列（分组信息）
                line = lines[i]
                # 如果第一列信息不为零，那么这是一个新的分组的开始
                if not line[0] == '':
                    # 先保存上一步信息
                    # 如果不是sheet最开始，就保存上一个分组统计的全部信息
                    if not gflag:
                        if totalcount == totalpass:
                            status = 'PASS'
                        else:
                            status = 'FAIL'
                        groupinfo.append(totalcount)
                        groupinfo.append(totalpass)
                        groupinfo.append(status)
                        self.groups.append(groupinfo)

                        # 重置下一个分组的统计信息
                        # 每一个分组统计信息为列表
                        groupinfo = []
                        # 统计每一个分组的用例总条数
                        totalcount = 0
                        # 统计分组用例中通过用例的条数数
                        totalpass = 0

                    # 保存分组名字
                    groupinfo.append(line[0])

                    # 表示当前分组未统计完
                    gflag = False

                # 第二列（类别或用例名）不同时为空,则不是用例，执行非用例的操作
                elif not line[1] == '':
                    # 不做统计
                    pass

                # 非用例行判断结束
                # 第一列信息和第二列信息均为空的行，即用例行，这时开始进行用例数、通过数、状态的统计。
                else:
                    # 判断执行结果列，如果为空，将flag置为false,视为该行有误，不纳入用例数量计算
                    if len(line) < 7 or line[7] == '':
                        pass
                    # 执行结果不为空，则将用例统计数自增
                    else:
                        totalcount = totalcount + 1
                        # logger.info(line)
                        # 如果通过，则通过数和总通过数均自增
                        if line[7] == "PASS":
                            totalpass += 1

            # 当一个sheet统计完成后，保存最后一个分组统计的结果
            if totalcount == totalpass:
                status = 'PASS'
            else:
                status = 'FAIL'

            if len(groupinfo) == 0:
                groupinfo.append('用例数据错误')
            groupinfo.append(totalcount)
            groupinfo.append(totalpass)
            groupinfo.append(status)
            self.groups.append(groupinfo)
            # 重置下一个分组的统计信息
            # 每一个分组统计信息为列表
            groupinfo = []

            # 统计每一个分组的用例总条数
            totalcount = 0
            # 统计分组用例中通过用例的条数数
            totalpass = 0

        return self.groups


if __name__ == '__main__':
    res = Result()
    r = res.get_res(path + 'lib/result-电商项目用例.xlsx')
    print(r)

    r = res.get_groups(path + 'lib/result-电商项目用例.xlsx')
    print(r)
