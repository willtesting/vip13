# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/13 20:58
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：参数获取
"""
params = "username=Will' or 1=1 #"
# 找到第一个=号的位置，需要兼容，字符串可能没有=号的情况
index = params.find('=')
print(index)

if index>0:
    key = params[:index]
    value = params[index+1:]
else:
    key = params
    value = ''





