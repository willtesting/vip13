# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/13 21:32
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：注册
"""
import requests

res = requests.post('http://testingedu.com.cn:8081/inter/HTTP/auth')
print(res.text)
token = res.json().get('token')

res = requests.post('http://testingedu.com.cn:8081/inter/HTTP/register',
                    data={'username': 'Tester26', 'pwd': '123456', 'nickname': '测试账号', 'describe': '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567'},
                    headers={'token':token})
print(res.text)






