# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/14 20:24
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : pip install pytest


# 测试框架 != 自动化框架  == 单元测试框架  pytest

# pytest识别的方式：如果希望某个地方需要执行，如果是函数 test_   *_test
# 如果是测试类，需要将Test  大写T  Test_xxx
import pytest


def test_case1():
    """测试用例函数"""
    print("这是测试用例1")

def test_case2():
    """测试用例函数"""
    print("这是测试用例2")

def case5():
    """测试用例函数"""
    print("这是测试用例5")

class Test_Obj:
    """测试类"""
    def test_case3(self):
        """测试用例函数"""
        print("这是测试用例3")

    def test_case4(self):
        """测试用例函数"""
        print("这是测试用例4")

# 第一种执行方式：通过pycharm 的 pytest插件来识别并执行

# 第二种执行方式：通过主函数来执行  pytest.main(['-s'])    -s:可以在控制台显示执行结果
# 通常为了方便，可以以外部文件去运行，相当于写一个运行入口

# 第三种执行方式：通过命令行的方式执行，pytest 文件路径/测试文件名




