# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/14 21:37
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : allure测试报告


# 1. 改allure logo
#  /allure/config/allur.yml: 最后加入一行 - custom-logo-plugin
#  plugins/custom-logo-plugin/static ，先把图标准备好，放到这里，然后编辑style.css
import time

import allure
import pytest

from vip_class11.webkeys import Web

@allure.feature('#1 电商测试用例demo')
class Test_Demo:
    def setup_class(self):
        self.web = Web()
        self.web.open_browser()

    @allure.story('#1 登录的用例')
    def test_login(self):
        allure.dynamic.title('成功的登录')
        allure.dynamic.description('识别验证码的登录')
        self.web.get_url('http://testingedu.com.cn:8000/index.php/Home/user/login.html')
        self.web.input('//*[@id="username"]', '2929184523@qq.com')
        self.web.input('//*[@id="password"]', '123456')
        with allure.step('验证码的破解'):
            ver = self.web.get_verfiy('//*[@id="verify_code_img"]')
            self.web.input('//*[@id="verify_code"]', ver)
        try:
            self.web.click('//*[@id="loginform"]/div/div[6]/a')
            allure.attach(self.web.driver.get_screenshot_as_png(), '执行成功', allure.attachment_type.PNG)
        except:
            allure.attach(self.web.driver.get_screenshot_as_png(), '执行失败', allure.attachment_type.PNG)
            # 认为测试用例执行失败
            pytest.fail()
        time.sleep(3)

    @allure.story('#2 搜索的用例')
    def test_search(self):
        allure.dynamic.title('搜索')
        self.web.input('//*[@id="q"]', '手机')
        self.web.click('//*[@id="sourch_form"]/a')

    def teardown_class(self):
        time.sleep(3)
        self.web.driver.quit()

