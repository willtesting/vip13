# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/14 20:46
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述
import os

import pytest

if __name__ == '__main__':
    # os.system('rd /s/q result')
    # os.system('rd /s/q result')

    pytest.main(['-s', 'test_demo3.py', '--alluredir', './result'])
    os.system('allure generate result -o report --clean')
