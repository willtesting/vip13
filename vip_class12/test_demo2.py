# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/14 21:11
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : pytest组织测试用例


class Test_Demo:
    def setup_class(self):
        # 类级别的前置处理
        print('类级别的前置')

    def setup(self):
        # 测试用例的前置条件
        # 如： web = Web()
        # web.open_browser()
        # ...
        print('所有测试用例执行前，都需要先执行这里...')
    def test_case1(self):
        print('test_case1')

    def test_case2(self):
        print('test_case2')

    def test_case3(self):
        print('test_case3')

    def teardown(self):
        # 测试用例的后置处理
        print('所有测试用例执行后，都需要执行这里...')

    def teardown_class(self):
        print('这是类级别的后置处理')

