# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/30 19:59
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# 指定了默认导入包里面，decs.py这模块
__all__ =['decs','demo1']
# 这里面的代码，在导入的时候就会被执行
# print(__all__)