# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/30 21:23
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：基础函数模块
"""
from class06.decs import my_time


@my_time
def add(*args):
    """
    加法
    :param args:
    :return:
    """
    s = 0
    for i in args:
        s += i
    return s


def minus(*args):
    """
    减法
    :param args:
    :return:
    """
    if args:
        s = args[0]
        for i in range(1, len(args)):
            s -= args[i]

        return s
    else:
        return 0


def chufa(*args):
    print('跑了')
    if args:
        s = args[0]
        for i in range(1, len(args)):
            if args[i]:
                s /= args[i]
            else:
                return '除数不能为0'

        return s
    else:
        return 0


# print(__name__)

if __name__ == '__main__':
    # 存放模块本身的测试代码，在模块被导入的时候，它是不跑的
    # 测试代码
    add(1,2,3)
