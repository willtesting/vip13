# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/30 21:24
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：装饰器模块
"""
import time


def my_time(func):
    """
    装饰器模板，并没有实现功能
    :param func: 是被装饰函数本身
    """

    # 说明是被装饰函数本身
    # print(func.__name__)

    def wrapper(*args, **kwargs):
        # 函数执行前
        t1 = time.time()

        # 调用被装饰函数
        res = func(*args, **kwargs)

        # 函数执行后
        t2 = time.time()
        print(func.__name__, '耗时', t2 - t1)

        return res

    return wrapper


def my_time_new(t='ms'):
    """最外层接收装饰器本身的参数"""

    def outer(func):
        """外层接收被装饰函数本身"""

        def wrapper(*args, **kwargs):
            """接收被装饰函数的参数"""
            t1 = time.time()
            res = func(*args, **kwargs)
            t2 = time.time()
            if t == 's':
                print(func.__name__, '耗时', round(t2 - t1, 2), t)
            else:
                print(func.__name__, '耗时', round((t2 - t1) * 1000, 2), t)
            return res

        return wrapper

    return outer

