# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 20:04
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：装饰器
"""
import time


# 比较函数的执行效率
def get_time(t='ms'):
    def outer(func):
        def wrapper(*args, **kwargs):
            t1 = time.time()
            res = func(*args, **kwargs)
            t2 = time.time()
            if t == 'ms':
                dt = round((t2 - t1) * 1000, 2)
            else:
                dt = round(t2 - t1, 2)
            print('函数：{}，执行耗时{} {}'.format(func.__name__, dt, t))
            return res

        return wrapper

    return outer
