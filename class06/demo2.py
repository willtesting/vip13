# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/30 21:57
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：格式化
"""
import math

x, y = 1, 2

print('x=' + str(x) + ', y=' + str(y))
# 变量填坑
print(f'x={x}, y={y}')
print('x={}, y={}'.format(x, y))


# 小数前后都可以补0
print("PI=%07.03f" % 5.2)
