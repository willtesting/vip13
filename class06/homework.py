# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 20:03
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：排序库
"""
from class06.home_decs import get_time


def quick_sort(height, left, right):
    """
    快排
    :param height: 待排序列表
    :param left: 递归的最左边
    :param right: 递归的最右边
    :return:
    """
    l = left
    h = right

    # 选最后作为基准
    base = height[right]

    while l < h:

        # 从左往右找比基准大的
        # 没找到的话，会一直找
        while l < h:
            # 如果找到了
            if height[l] > base:
                # 把l,和h的元素交换，h-1
                height[l], height[h] = height[h], height[l]
                h -= 1
                # 一旦找到了，就进行另一个方向的寻找
                break
            else:
                l += 1

        # 从右往左找比基准小的
        # 没找到的话，会一直找
        while l < h:
            # 如果找到了
            if height[h] < base:
                # 把l,和h的元素交换，l+=1
                height[l], height[h] = height[h], height[l]
                l += 1
                # 一旦找到了，就进行另一个方向的寻找
                break
            else:
                h -= 1

    # print(height)

    # 递归（left 到 l -1）
    if l - 1 > left:
        quick_sort(height, left, l - 1)

    # （h+1，right）
    if right > h + 1:
        quick_sort(height, h + 1, right)


@get_time()
def bubble_sort(height):
    """
    冒泡排序
    :param height: 代排序列表
    """
    j = 0
    while j < len(height) - 1:
        # 第一轮
        i = 0
        # -j的意思是，每一轮都少比一个元素
        while i < len(height) - 1 - j:
            if height[i] > height[i + 1]:
                # 交换
                height[i], height[i + 1] = height[i + 1], height[i]

            i += 1

        j += 1


@get_time(t='s')
def select_sort(height):
    """
    选择排序
    :param height: 待排序列表
    :return:
    """
    j = 0
    while j < len(height) - 1:
        # 第一个不用比
        i = 1
        # 记录最大的下标
        max = 0
        while i < len(height) - j:
            # 两两比较。记录较大值的下标
            if height[i] > height[max]:
                max = i

            i += 1

        # 把最大的交换到末尾
        height[max], height[len(height) - 1 - j] = height[len(height) - 1 - j], height[max]
        # print(height)
        j += 1

    return height


if __name__ == '__main__':
    h = [i for i in range(5000, 0, -1)]
    bubble_sort(h)

    h = [i for i in range(5000, 0, -1)]
    select_sort(h)

    # # 调整堆栈大小
    # import sys
    # sys.setrecursionlimit(10 ** 6)
    #
    # h = [i for i in range(2000, 0, -1)]
    # quick_sort(h,0,len(h)-1)

