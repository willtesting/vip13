# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/5 20:34
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：项目地址：http://39.108.55.18:8000/Home/user/login.html
"""
import time
from selenium import webdriver


# 打开浏览器
driver = webdriver.Chrome()

# 隐式等待
driver.implicitly_wait(10)

# driver对象，提供了一系列操作网页的函数（功能）
driver.get('http://39.108.55.18:8000/Home/user/login.html')

# id定位
ele = driver.find_element('id','username')
ele.send_keys('13800138006')
# name定位
driver.find_element('name','password').send_keys('123456')
# classname定位
ele = driver.find_element('class name','text_cmu')
print(ele.get_attribute('outerHTML'))

# tagname定位
ele = driver.find_element('tag name','input')
print(ele.get_attribute('outerHTML'))

driver.find_element('id','verify_code').send_keys('1111')

# a标签文本定位（文本类最好是复制）
# 如果文本有回车，请用包含
ele = driver.find_element('link text','登    录')
print(ele.get_attribute('outerHTML'))
# a标签的文本包含
ele = driver.find_element('partial link text','登')
print(ele.get_attribute('outerHTML'))

driver.find_element('xpath','//*[@id="loginform"]/div/div[6]/a').click()

# 断言昵称
ele = driver.find_element('link text','bkpp')
print(ele.text)


