# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/5 21:12
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""


# 指定函数的返回值类型
def funcs(s: str = '') -> list[str]:
    print(s)
    return ['1', '2']


print(funcs(111))
