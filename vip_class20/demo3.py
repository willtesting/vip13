# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/6 21:30
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：application/json;格式参数
"""

# 把json字符串转成字典
import json
from urllib import parse

import requests

session = requests.session()

param = '{"username":"Will","pwd":"123456"}'
param = json.loads(param)

# http://39.108.55.18/mypro/index.html#/home
res = session.post('http://39.108.55.18/mypro/api/user/login',json=param)
print(res.text)
print(res.request)


res = session.get('http://39.108.55.18/mypro/api/res/exportres?id=25')
# 获取文件名：url解码和编码
filename = res.headers.get('Content-Disposition')
print(filename)
filename = parse.unquote(filename)
print(filename)
