# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/6 21:34
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import requests

parma = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://soap.testingedu.com/"><soapenv:Header/><soapenv:Body><soap:login><arg0>Will</arg0><arg1>123456</arg1></soap:login></soapenv:Body></soapenv:Envelope>'
res = requests.post('http://testingedu.com.cn:8081/inter/SOAP?wsdl',data=parma)
print(res.text)