# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/6 20:57
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：接口调用
"""
import json

import requests


# # 调用接口
# res = requests.get('https://sp1.baidu.com/8aQDcjqpAAV3otqbppnN2DJv/api.php?query=11.111.11.11&co=&resource_id=5809&t=1673009867481&ie=utf8&oe=gbk&cb=op_aladdin_callback&format=json&tn=baidu&cb=jQuery110206964295285362558_1673007949295&_=1673007949307')
# print(res.text)

parma = {
    'query': '113.246.108.37',
    'co':'',
    'resource_id': 5809,
    't': 1673009938746,
    'ie': 'utf8',
    'oe': 'gbk',
    'format': 'json',
    'tn': 'baidu',
    'cb': 'jQuery110206964295285362558_1673007949295',
    '_': 1673007949308
}
res = requests.get('https://sp1.baidu.com/8aQDcjqpAAV3otqbppnN2DJv/api.php',params=parma)
# print(res.text)
# 二进制
# print(res.content.decode('unicode-escape'))
# print(res.json())
res = res.text
res = res[res.find('{'):res.rfind('}')+1]
print(res)
# print(json.loads(res))

# print(res.request.url)
# print(res.apparent_encoding)