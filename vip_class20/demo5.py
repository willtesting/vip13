# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/6 21:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：文件上传与下载
"""
import requests

# 接口操作文件，都是操作文件的二进制
file = {'image': open('../lib/will.png',mode='rb')}
res = requests.post('https://graph.baidu.com/upload',files=file,verify=False)
print(res.text)

res = requests.get('https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202109%2F06%2F20210906225922_1c31b.thumb.1000_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1675605425&t=28f1af2502bba8c9e4756203d953df8f')
# 图片的二进制，把它写到文件里面，就下载下来了
file = open('../lib/girls.png',mode='wb')
file.write(res.content)
file.close()
