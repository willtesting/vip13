# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/6 21:24
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：application/x-www-form-urlencoded格式
"""
import requests


def url_to_data():
    """url格式参数转字典"""


param = 'username=Will&password=123456'
param = {
    'username': 'Will',
    'password': '123456'
}

res = requests.post('http://testingedu.com.cn:8081/inter/HTTP/login',data=param)
print(res.text)





