# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 20:27
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 基类的封装
import logging
import re

from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.select import Select

from vip_class13.common.zsq import relations

logger = logging.getLogger('PO')

class BasePage:
    """Web自动化PO模式 基类"""
    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.relation_dict = {}

    def get_url(self, url=''):
        """打开被测地址"""
        self.driver.get(url)

    def __find_element(self, locator: str = ''):
        """
        统一定位方式
        :param locator: 元素定位器，同时支持xpath/css/id
        :return: 定位到的元素，如果没有就返回None
        """
        logger.info(f'开始寻找元素:{locator}...')
        try:
            if locator is None or locator == '':
                return None
            # xpath通常以'/'开头，可能也会以'('开头:
            elif locator.startswith('/') or locator.startswith('('):
                logger.info(f'通过xpath方式，{locator} 查找成功... ')
                element = self.driver.find_element('xpath', locator)
            elif locator.startswith('#') or locator.__contains__('>'):
                logger.info(f'通过css selector方式，{locator} 查找成功... ')
                element = self.driver.find_element('css selector', locator)
            else:
                logger.info(f'通过id方式，{locator} 查找成功... ')
                element = self.driver.find_element('id', locator)
            # 给操作的元素高亮显示
            if element:
                self.driver.execute_script("arguments[0].style.background = '#00ff50'", element)
            return element
        except:
            logger.error(f'查找元素 {locator} 失败...')
            raise

    @relations
    def input(self, locator='', value=''):
        """
        文本输入
        :param locator: 元素定位
        :param value: 输入的内容
        :return:
        """
        logger.info(f'开始在元素 {locator} 输入 {value} ...')
        try:
            element = self.__find_element(locator)
            element.send_keys(value)
            logger.info(f'在元素 {locator} 输入 {value} 成功...')
        except:
            logger.error(f'在元素 {locator} 输入 {value} 失败...')
            raise

    def clear(self, locator):
        """清空输入框"""
        element = self.__find_element(locator)
        element.clear()

    def click(self, locator):
        """
        点击元素
        :param locator:
        :return:
        """
        logger.info(f'开始点击元素 {locator} ...')
        try:
            logger.info(f'点击元素 {locator} 成功...')
            element = self.__find_element(locator)
            element.click()
        except:
            logger.error(f'点击元素 {locator} 失败...')
            raise

    def js_click(self, locator=''):
        """js点击"""
        element = self.__find_element(locator)
        self.driver.execute_script("arguments[0].click()", element)

    def try_clicks(self, locator=''):
        """点击"""
        try:
            self.driver.implicitly_wait(3)
            element = self.__find_element(locator)
            element.click()
        except Exception as e:
            print('尝试点击元素 %s 错误' % locator)
        finally:
            self.driver.implicitly_wait(10)

    def get_text(self, locator='', reg=''):
        """
        获取元素的文本
        :param locator:
        :param reg: 对文本进行正则处理
        :return:
        """
        element = self.__find_element(locator)
        text = element.text
        # 如果有正则，就按正则来处理
        if reg:
            text = re.findall(reg, text)
            if text:
                text = text[0]

        # 如果获取的属性可以是一个系统变量，则用{text}保存起来
        self.relation_dict['text'] = text
        return text

    @relations
    def save_params(self, name='', value=''):
        """
        把参数保存为你需要的名字
        :param name: 参数名
        :param value: 参数值
        :return:
        """
        self.relation_dict[name] = value

    def refresh(self):
        """刷新页面"""
        self.driver.refresh()

    def into_iframe(self, locator):
        """进入iframe"""
        element = self.__find_element(locator)
        self.driver.switch_to.frame(element)

    def out_iframe(self):
        """退出iframe"""
        self.driver.switch_to.default_content()

    def select(self, locator, value):
        """
        下拉框处理
        :param locator: 下拉框的定位
        :param value: 选择的值
        :return:
        """
        element = self.__find_element(locator)
        select = Select(element)
        try:
            int(value)
            select.select_by_value(value)
        except:
            select.select_by_visible_text(value)

    def move_to(self, locator):
        """鼠标悬停"""
        element = self.__find_element(locator)
        action = ActionChains(self.driver)
        # 如果不加perform，不会生效
        action.move_to_element(element).perform()

    def switch_window(self):
        """切换窗口"""
        handels = self.driver.window_handles
        print(handels)
        self.driver.close()
        self.driver.switch_to.window(handels[1])
