# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 20:50
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 登录页面封装
from vip_class13.common.base_page import BasePage

"""
在页面对象种，考虑以下内容：
1.元素的定位信息
2.测试用例执行的方法（步骤）
3.是否考虑严格的PO原则--->返回其他PO对象？
"""


class LoginPage(BasePage):
    LoginPage_locators = {
        'input_username_locator': '//*[@id="username"]',
        'input_password_locator': '//*[@id="password"]',
        'input_verify_locator': '//*[@id="verify_code"]',
        'login_button_locator': '//a[@name="sbtbutton"]',
    }

    def login(self, username, password, verify):
        self.refresh()
        self.clear(self.LoginPage_locators['input_username_locator'])
        self.input(self.LoginPage_locators['input_username_locator'], username)
        self.clear(self.LoginPage_locators['input_password_locator'])
        self.input(self.LoginPage_locators['input_password_locator'], password)
        self.clear(self.LoginPage_locators['input_verify_locator'])
        self.input(self.LoginPage_locators['input_verify_locator'], verify)
        self.click(self.LoginPage_locators['login_button_locator'])

    # def assert_login_state