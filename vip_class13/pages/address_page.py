# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 21:41
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 地址管理页面
from vip_class13.common.base_page import BasePage


class AddressPage(BasePage):
    AddressPage_locators = {
        'add_address_locator': '//span[@class="co_blue"]',
        'user_name_locator': '//input[@name="consignee"]',
        'user_phone_locator': '//input[@name="mobile"]',
        'province_locator': '//*[@id="province"]',
        'city_locator': '//*[@id="city"]',
        'district_locator': '//*[@id="district"]',
        'town_locator': '//*[@id="twon"]',
        'detailed_address_locator': '//input[@name="address"]',
        'submit_locator': '//*[@id="address_submit"]',
    }

    def add_address(self, username, phone, province, city, district, town, d_address):
        self.get_url('http://testingedu.com.cn:8000/Home/User/address_list.html')
        self.click(self.AddressPage_locators['add_address_locator'])
        self.input(self.AddressPage_locators['user_name_locator'], username)
        self.input(self.AddressPage_locators['user_phone_locator'], phone)
        self.select(self.AddressPage_locators['province_locator'], province)
        self.select(self.AddressPage_locators['city_locator'], city)
        self.select(self.AddressPage_locators['district_locator'], district)
        self.select(self.AddressPage_locators['town_locator'], town)
        self.input(self.AddressPage_locators['detailed_address_locator'], d_address)
        self.click(self.AddressPage_locators['submit_locator'])

    def del_address(self, username):
        self.click(f'//span[text()="{username}"]/../..//a[text()="删除"]')

