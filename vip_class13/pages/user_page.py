# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 21:34
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 用户信息页面
from vip_class13.common.base_page import BasePage


class UserPage(BasePage):
    UserPage_locators = {
        'user_img_locator': '//*[@id="preview"]',
        'iframe_locator': '//*[@id="layui-layer-iframe1"]',
        'upload_locator': '//div[@id="filePicker"]/div[2]/input',
        'use_img_locator': '//div[@class="saveBtn"]',
        'save_img_locator': '//input[@class="save"]',
    }

    def change_user_img(self, path):
        self.get_url('http://testingedu.com.cn:8000/Home/User/info.html')
        self.click(self.UserPage_locators['user_img_locator'])
        self.into_iframe(self.UserPage_locators['iframe_locator'])
        self.input(self.UserPage_locators['upload_locator'], path)
        self.click(self.UserPage_locators['use_img_locator'])
        self.out_iframe()
        self.click(self.UserPage_locators['save_img_locator'])
