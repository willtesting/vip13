# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 21:52
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 冒烟测试用例
from time import sleep

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from vip_class13.pages.address_page import AddressPage
from vip_class13.pages.login_page import LoginPage
from vip_class13.pages.user_page import UserPage
from vip_class13.testcases import conftest


class TestLogin:
    # def setup_class(self):
    #     option = Options()
    #     # 去掉自动化标识
    #     option.add_experimental_option('excludeSwitches', ['enable-automation'])
    #     option.add_argument('--disable-blink-features=AutomationControlled')
    #     # 关掉密码弹窗
    #     prefs = {}
    #     prefs['credentials_enable_service'] = False
    #     prefs['profile.password_manager_enabled'] = False
    #     option.add_experimental_option('prefs', prefs)
    #     self.driver = webdriver.Chrome(options=option)
    #     # 添加隐式等待
    #     self.driver.implicitly_wait(15)
    #     # 最大化浏览器
    #     self.driver.maximize_window()
    #     self.driver.get('http://testingedu.com.cn:8000/Home/user/login.html')

    # def test_login(self):
    #     self.login_page = LoginPage(self.driver)
    #     self.login_page.login('2929184523@qq.com', '123456', '1111')
    #     sleep(3)

    def test_change_img(self,get_driver):
        self.driver = get_driver
        print('#############',conftest.a)
        self.user_page = UserPage(self.driver)
        self.user_page.change_user_img(r'/usr/local/yomi.png')

    def test_manage_address(self,get_driver):
        self.driver = get_driver
        self.address_page = AddressPage(self.driver)
        self.address_page.add_address('zxtest',
                                      '17777777777',
                                      '湖南省',
                                      '长沙市',
                                      '25607',
                                      '25612',
                                      'zx测试地址')
        sleep(3)
        self.address_page.del_address('zxtest')
        sleep(3)

    # def teardown_class(self):
    #     self.driver.quit()
