# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/19 20:24
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：pytest的注入文件
"""
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

a = 1111


@pytest.fixture(scope="session")
def get_driver():
    """打开浏览器组件写法
    组件不能在setup和teardown里面使用
    """
    print('###########################打开前')
    option = Options()
    # 去掉自动化标识
    option.add_experimental_option('excludeSwitches', ['enable-automation'])
    option.add_argument('--disable-blink-features=AutomationControlled')
    # 关掉密码弹窗
    prefs = {}
    prefs['credentials_enable_service'] = False
    prefs['profile.password_manager_enabled'] = False
    option.add_experimental_option('prefs', prefs)
    driver = webdriver.Chrome(options=option)
    # 添加隐式等待
    driver.implicitly_wait(15)
    # 最大化浏览器
    driver.maximize_window()
    yield driver
    print('###########################打开完成')
    driver.quit()





