# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 21:17
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 登录测试用例
from time import sleep

import pytest
import yaml
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from vip_class13.pages.login_page import LoginPage


class TestLogin:
    # def setup_class(self,get_driver):
    #     option = Options()
    #     # 去掉自动化标识
    #     option.add_experimental_option('excludeSwitches', ['enable-automation'])
    #     option.add_argument('--disable-blink-features=AutomationControlled')
    #     # 关掉密码弹窗
    #     prefs = {}
    #     prefs['credentials_enable_service'] = False
    #     prefs['profile.password_manager_enabled'] = False
    #     option.add_experimental_option('prefs', prefs)
    #     self.driver = webdriver.Chrome(options=option)
    #     # 添加隐式等待
    #     self.driver.implicitly_wait(15)
    #     # 最大化浏览器
    #     self.driver.maximize_window()
    #     self.driver = get_driver
    #     self.driver.get('http://testingedu.com.cn:8000/Home/user/login.html')

    # @pytest.mark.parametrize('u, p, v', [
    #     ('2929184523@qq.com', '123123', '1111'),
    #     ('2929184523@qq.com', '123456', '1111'),
    # ])
    userdata = yaml.safe_load(open('userdata.yaml', 'r', encoding='utf8'))

    @pytest.mark.parametrize('u, p, v', userdata)
    def test_login(self, u, p, v,get_driver):
        # self.index_page = IndexPage(self.driver)
        # self.login_page = self.index_page.to_login_page()
        self.driver = get_driver
        self.driver.get('http://testingedu.com.cn:8000/Home/user/login.html')
        self.login_page = LoginPage(self.driver)
        self.login_page.login(u, p, v)
        sleep(2)

    # def teardown_class(self):
    #     self.driver.quit()

