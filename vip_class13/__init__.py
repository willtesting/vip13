# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/16 20:01
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述
from logging.config import dictConfig

config = {
    'version': 1,
    'formatters': {
        'detailed': {
            'class': 'logging.Formatter',
            'format': '%(levelname)s %(asctime)s %(message)s'
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'filename': '../outPut/log/test.log',
            'mode': 'w',
            'encoding': 'utf8',
            'formatter': 'detailed',
            'level': 'INFO'
        },
    },
    'loggers': {
        'PO': {
            'handlers': ['file'],
            'level': 'INFO'
        }
    }
}

dictConfig(config)