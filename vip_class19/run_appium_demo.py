# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/4 20:35
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：启、停appium的服务
"""
import os
import threading
import time

# 为线程创建一个函数，其实就是告诉这个线程，你要做什么
def run_cmd(cmd):
    print('开始运行', cmd)
    os.system(cmd)


appium_path = r'E:\software\Appium'

port = '4724'

# cmd = fr'node "{appium_path}\resources\app\node_modules\appium\build\lib\main.js" -p {port}'
# # 使用多线程运行appium
# th = threading.Thread(target=run_cmd,args=(cmd,))
# th.start()
#
# time.sleep(6)
# 停止appium
res = os.popen(f'netstat -aon | findstr {port}')
# print(res.read())

pid = res.read().strip().split('\n')[0].split(' ')
print(pid)
pid = pid[-1]
cmd = f'taskkill /F /pid {pid}'
print(cmd)
os.system(f'taskkill /F /pid {pid}')