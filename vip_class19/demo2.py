# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/4 21:31
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# # IO密集
# a = input('输入：')
# print(a)
import json

conf = """
{
  "platformName": "Android",
  "deviceName": "127.0.0.1:7555",
  "appPackage": "com.tencent.mobileqq",
  "appActivity": ".activity.SplashActivity",
  "noReset": "true",
  "automationname": "uiautomator1"
}"""
conf = json.loads(conf)
print(conf)



