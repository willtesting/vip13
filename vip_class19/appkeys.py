# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/4 20:28
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：app关键字驱动
"""
import json
import os
import threading
import time

from vip_class11.zsq import relations
from vip_class16.common.Logger import logger

from appium import webdriver


class App:

    def __init__(self):
        # 指定driver的类型，方便面对对象封装，可以点出方法来
        self.driver: webdriver.Remote = None
        # 记录appium的端口
        self.port = '4723'
        # 关联
        self.relation_dict = {}

    def runappium(self, port='4723', appium_path='E:\software\Appium'):
        """启动appium服务"""

        # 为线程创建一个函数，其实就是告诉这个线程，你要做什么
        def run_cmd(cmd):
            logger.info('开始运行appium')
            logger.info(cmd)
            os.system(cmd)

        if not appium_path:
            appium_path = r'E:\software\Appium'

        if not port:
            port = '4723'
        else:
            self.port = port

        cmd = fr'node "{appium_path}\resources\app\node_modules\appium\build\lib\main.js" -p {port}'
        # 使用多线程运行appium
        th = threading.Thread(target=run_cmd, args=(cmd,))
        th.start()
        time.sleep(3)

    def stopappium(self):
        # 停止appium
        cmd = f'netstat -aon | findstr {self.port}'
        logger.info(cmd)
        res = os.popen(cmd)
        pid = res.read().strip().split('\n')[0].split(' ')
        logger.info(pid)
        pid = pid[-1]
        cmd = f'taskkill /F /pid {pid}'
        logger.info(cmd)
        os.system(f'taskkill /F /pid {pid}')

    def runapp(self, conf='''{
          "platformName": "Android",
          "deviceName": "127.0.0.1:7555",
          "appPackage": "com.tencent.mobileqq",
          "appActivity": ".activity.SplashActivity",
          "noReset": "true",
          "automationname": "uiautomator1"
        }'''):
        conf = json.loads(conf)
        # 指定运行的设备
        conf['uuid'] = conf.get('deviceName')
        self.driver = webdriver.Remote(f"http://127.0.0.1:{self.port}/wd/hub", conf)
        self.driver.implicitly_wait(10)

    def __find_ele(self, locator=""):
        """找元素，兼容，id、accessibility id、xpath"""
        if locator.startswith('/'):
            ele = self.driver.find_element('xpath', locator)
        elif locator.__contains__(':id/'):
            ele = self.driver.find_element('id', locator)
        else:
            ele = self.driver.find_element('accessibility id', locator)

        return ele

    @relations
    def input(self, locator="", text=""):
        ele = self.__find_ele(locator)
        ele.send_keys(text)

    def click(self, locator=""):
        ele = self.__find_ele(locator)
        ele.click()

    def sleep(self, t='1'):
        try:
            t = float(t)
        except:
            t = 1

        time.sleep(t)

    def keycode(self, code='66'):
        """
        按键
        :param code: 默认66，回车键
        """
        try:
            code = int(code)
        except:
            code = 66
        self.driver.keyevent(code)

    def quit(self):
        self.driver.quit()
        self.driver = None
