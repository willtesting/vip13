# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/4 21:05
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：多线程
"""
import threading


# 为线程创建一个函数，其实就是告诉这个线程，你要做什么
def run_cmd(cmd):
    print('开始运行', cmd)


# 创建线程
# target是线程需要做事情的函数
# args，是函数需要用到的参数
th = threading.Thread(target=run_cmd,args=('hello',))
# 运行线程
th.start()