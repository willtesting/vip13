# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 20:38
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数的传参
"""


def my_func1(x, y, z, *args, a='11', b='22', **kwargs):
    """
    函数传参详解
    总体来说，函数一共只有两种参数类型：位置型参数，键值对参数
    x,y,z：显示定义的位置型参数
    *args：把所有没有显示定义的位置型参数接收为一个元组
    a,b：显示定义键值对参数，也叫带默认值的参数，如果不传就使用默认值，如果传了就是用传了的值
    **kwargs：接收所有没有显示定义的键值对参数，接收为一个字典
    理论上：使用*args和**kwargs，可以接收所有参数
    """
    print(x, y, z)
    # 元组
    print(args)
    print(a, b)
    # 字典
    print(kwargs)


my_func1(1, 2, 3, 4, 5, 6, a='aa', b='bb', c='cc', d='dd')


def my_func2(*args, **kwargs):
    """
    拓展
    函数功能，把位置型参数求和，把键值对参数+1
    :param args: 使用*args，可以接收所有线性数据结构参数
    :param kwargs: 使用**kwargs，可以接收所有的键值对结构参数
    """
    print(args, kwargs)
    # 求和
    s = 0
    for i in args:
        s += i

    # 加1
    for key in kwargs.keys():
        kwargs[key] += 1

    return s, kwargs


# 线性数据结构传参
l = [i for i in range(10)]
print(l)
# 使用*l，代表把l线性数据结构里面的元素，迭代的写入到函数的参数列表
# my_func2(l[0],l[1],……)
s = my_func2(*l)
print(s)

# 键值对参数也可以这样传
d = {'a': 1, 'b': 2, 'c': 3}
s = my_func2(**d)
print(s)




