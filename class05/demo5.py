# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 21:19
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数的传值与传地址
"""


# # 传值
# a = 1
#
#
# def my_func1(a1):
#     a1 += 1
#     print(a1)
#     return a1
#
#
# # a-实参，x-形参
# my_func1(a)
# print(a)


def my_func2(list_a1):
    # 传地址
    list_a1[1] = 22
    print('list_a1',list_a1)

    # 传值（就是看容器变量是否重新指向了新的容器）
    list_a1 = [1, 2, 3]
    print(id(list_a1))
    list_a1[1] = 222
    print('list_a1', list_a1)


# 传地址
list_a = [1, 2, 3]
print(id(list_a))
my_func2(list_a)
print('list_a', list_a)


# 列表，字典，我们称为容器变量（实际上，存是的指针）
def my_func3(d1):
    d1['a'] = 33
    print(d1)


d = {'a': 1, 'b': 2}
my_func3(d)
print(d)
