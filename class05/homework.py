# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/30 19:59
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""


def quick_sort(height, left, right):
    """
    快排
    :param height: 待排序列表
    :param left: 递归的最左边
    :param right: 递归的最右边
    :return:
    """
    l = left
    h = right

    # 选最后作为基准
    base = height[right]

    while l < h:

        # 从左往右找比基准大的
        # 没找到的话，会一直找
        while l < h:
            # 如果找到了
            if height[l] > base:
                # 把l,和h的元素交换，h-1
                height[l], height[h] = height[h], height[l]
                h -= 1
                # 一旦找到了，就进行另一个方向的寻找
                break
            else:
                l += 1

        # 从右往左找比基准小的
        # 没找到的话，会一直找
        while l < h:
            # 如果找到了
            if height[h] < base:
                # 把l,和h的元素交换，l+=1
                height[l], height[h] = height[h], height[l]
                l += 1
                # 一旦找到了，就进行另一个方向的寻找
                break
            else:
                h -= 1

    # print(height)

    # 递归（left 到 l -1）
    if l - 1 > left:
        quick_sort(height, left, l - 1)

    # （h+1，right）
    if right > h + 1:
        quick_sort(height, h + 1, right)


def bubble_sort(height):
    """
    冒泡排序
    :param height: 代排序列表
    """
    j = 0
    while j < len(height) - 1:
        # 第一轮
        i = 0
        # -j的意思是，每一轮都少比一个元素
        while i < len(height) - 1 - j:
            if height[i] > height[i + 1]:
                # 交换
                height[i], height[i + 1] = height[i + 1], height[i]

            i += 1

        j += 1


def select_sort(height):
    """
    选择排序
    :param height: 待排序列表
    :return:
    """
    j = 0
    while j < len(height) - 1:
        # 第一个不用比
        i = 1
        # 记录最大的下标
        max = 0
        while i < len(height) - j:
            # 两两比较。记录较大值的下标
            if height[i] > height[max]:
                max = i

            i += 1

        # 把最大的交换到末尾
        height[max], height[len(height) - 1 - j] = height[len(height) - 1 - j], height[max]
        # print(height)
        j += 1

    return height


# height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]
# quick_sort(height,0,len(height)-1)
# print(height)
#
# height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]
# bubble_sort(height)
# print(height)

# height = select_sort([155, 187, 172, 160, 163, 166, 173, 182, 165, 159])
# print(height)
# print(select_sort.__doc__)


def is_huiwen(s: str = ''):
    """
    判断一个字符串是否为回文字符串
    如果是，返回相反的两部分
    如果不是，返回False
    :param s: 目标字符串，通过冒号指定类型
    :return:
    """
    if s and s == s[::-1]:
        # 是回文字符串
        # 相反的两部分，分长度是奇数和偶数
        if len(s) % 2 == 0:
            return s[:len(s) // 2], s[len(s) // 2:]
        else:
            return s[:len(s) // 2 + 1], s[len(s) // 2:]
    else:
        return False


s = 'abba'
print(is_huiwen(1))
