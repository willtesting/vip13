# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 21:37
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：递归
"""


# # 反例
# def my_func():
#     my_func()
#
#
# my_func()

# n!（n的阶乘）：n! = n * (n-1)!，n=1,n!=1
def jie_c(n):
    # 出口：1！=1
    if n == 1:
        return 1

    # 递归实现n! = n * (n-1)!
    s = n * jie_c(n - 1)
    return s


s = jie_c(4)
print(s)
