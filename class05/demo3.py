# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 21:03
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：当位置型参数和键值对参数混传
"""


def my_func(x, y, a='aa', b='bb'):
    """
    函数并没有定义*args和**kwargs
    位置型参数，一定要在键值参数前面
    """
    print(x, y)
    print(a, b)


# 全位置型
my_func(1, 2, 3, 4)
# 键值对参数省略
my_func(1,2)
# 键值对和位置混传：位置型是必传，不传会报错
my_func(1,2,b='11')
# # 位置型参数，必须写在键值对参数前面
# my_func(1,b='11',2)
