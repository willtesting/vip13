# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 21:09
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数实战
"""


def my_re_n(s, old, new, n=1):
    """
    把一个字符串的第n个子串替换为新串
    :param s: 源字符串
    :param old: 需要替换的子串
    :param new: 替换的新串
    :param n: 需要替换第几个，n默认是1，替换第一个
    :return: 替换后的字符串
    """
    if n <= 0:
        # 不用替换，所以直接不用跑
        print(1)
        # return，代表，如果执行了return，就是函数执行结束，不管后面有没有代码，都不执行了
        return s

    if n == 1:
        # 如果是第一个，直接替换就行了
        s = s.replace(old,new,1)
        return s

    s = s.replace(old, '$$$roy', n)
    # 再把前n-1替换回去
    s = s.replace('$$$roy', old, n - 1)
    s = s.replace('$$$roy', new)
    print(2)
    return s


s = my_re_n('abcabcabc','a','b',3)
print(s)
