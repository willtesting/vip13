# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 21:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：快速排序
"""
height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]


def quick_sort(height, left, right):
    """
    快排
    :param height: 待排序列表
    :param left: 递归的最左边
    :param right: 递归的最右边
    :return:
    """
    l = left
    h = right

    # 选最后作为基准
    base = height[right]

    while l < h:

        # 从左往右找比基准大的
        # 没找到的话，会一直找
        while l < h:
            # 如果找到了
            if height[l] > base:
                # 把l,和h的元素交换，h-1
                height[l], height[h] = height[h], height[l]
                h -= 1
                # 一旦找到了，就进行另一个方向的寻找
                break
            else:
                l += 1

        # 从右往左找比基准小的
        # 没找到的话，会一直找
        while l < h:
            # 如果找到了
            if height[h] < base:
                # 把l,和h的元素交换，l+=1
                height[l], height[h] = height[h], height[l]
                l += 1
                # 一旦找到了，就进行另一个方向的寻找
                break
            else:
                h -= 1

    # print(height)

    # 递归（left 到 l -1）
    if l - 1 > left:
        quick_sort(height, left, l - 1)

    # （h+1，right）
    if right > h + 1:
        quick_sort(height, h + 1, right)


quick_sort(height, 0, len(height) - 1)
print(height)

