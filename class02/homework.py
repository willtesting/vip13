# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 17:08
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：作业
"""
# print('############################下面是结果###########################')
# # 将课程九九乘法表，以for循环实现
# # 两个数相乘，都是从1-9递增
# # 先控制x循环递增到9
# print('# 将课程九九乘法表，以for循环实现')
# for x in range(1, 10):
#     # 在循环里面，再控制y循环递增到9
#     # 每一个x都会和9个y相乘
#     # 定义每行显示的内容
#     for y in range(1, 10):
#         # 两条对角线的条件
#         if x == y or x + y == 10:
#             print('\t', end='\t')
#         else:
#             print(str(y) + "x" + str(x) + "=" + str(x * y),end='\t')
#     x += 1
#     print()
#
# print()
#
# # 四舍五入保留两位小数
# print('# 四舍五入保留两位小数')
# f = 100.000001
# print(round(f, 2))
# print()

# 用户名和密码都是字符串，长度为6-16位，如何判断一个用户名和密码长度是否合法？
# print('用户名和密码都是字符串，长度为6-16位，如何判断一个用户名和密码长度是否合法？')
# # 判断字符串长度的函数：len()
# loginname = input("请在此输入用户名：")
# password = input("请在此输入密码：")
# # loginname = None
# # 如果变量可能存在空的情况
# if loginname and password and 6 <= len(loginname) < 17 and 6 <= len(password) < 17:
#     print('用户名密码合法')
# else:
#     print('用户名密码非法')
#
# print()

# 九九乘法表去掉对角线
print('# 九九乘法表去掉对角线')
x = 1
while x < 10:
    y = 0
    while y < 9:
        y += 1
        # 第一条对角线条件是x==y
        # 第二条对角线条件是x+y==10
        # if x == y or x + y == 10:
        #     print('\t',end='\t')
        # else:
        if x == y or x + y == 10:
            print('\t', end='\t')
        else:
            print(str(y) + "x" + str(x) + "=" + str(x * y), end='\t')

    print()
    x += 1
