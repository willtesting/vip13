# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 20:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：逻辑运算（多个比较结果的组合结果）
"""
# 编程里面怎么表示x>3 而且 x < 100 ？
x = 5
print(3 < x < 100)
print(3 < x and x < 100)

# not>and>or，有括号先算括号里面
# and：真真为真，有假为假
print(False and True)
print(True and True)

# or：假假为假，有真为真
print(False or True)
print(False or False)

# not：取反
print(not False)

# 短路原则：当and 和 or可以通过左边的结果判断整个结果的时候，右边就不用执行了
# 短路原则可以提升执行效率
# 就相当于做单选题的时候，第一个选项已经确定是正确的了，后面的就可以不用看了
print(x < 1 and x > 100)
print(True or xxxx)

x, y = 3, 0
# 计算x/y 是不是大于1
if y != 0 and x / y > 1:
    print('是的')
else:
    print('不是的')
