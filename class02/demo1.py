# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 20:25
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：算术运算
"""
# 普通运算
x = 1 + 2 - 3 * 4 / 5 // 2 ** 2
print(x)

# 优先级
# 幂运算 > 乘，除，取余和取整除 > 加减，有括号先算括号里面的
x = 1 + 2 - (3 * 4 / 5 // (2 ** 2))
print(x)

# 四舍五入保留多少位小数
x = 3.355
y = round(x, 2)
print(y)
