# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 22:13
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：九九乘法表
"""
x = 1
while x < 10:
    # 每次x变化，y都从1~9
    y = 1
    while y < 10:
        # 得到三角形，当y>x，我循环不跑了，break就可以了
        if y > x:
            break

        print(str(x) + 'x' + str(y) + '=' + str(x * y), end=',\t')
        y += 1

    # 一行输完，我们来个换行
    print()
    x += 1
