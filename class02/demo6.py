# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 21:37
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：循环
"""

# i = 0
# while i < 3:
#     # 循环体代码
#     print('拿一个苹果，从A放到B')
#
#     # 一定要注意
#     # 每次循环改变控制条件
#     i += 1

m = 1
while m < 5:
    # 这是一周
    d = 1
    while d < 8:
        if d < 6:
            print('上班')
            # 改变条件
            d += 1
        else:
            # # 终止本层循环
            # break
            print(d)
            # 改变条件
            d += 1
            # 终止本次循环，循环其他次数还会正常继续，只是本次循环continue后面的代码不执行了
            # 所以使用while+continue的时候，要注意是否影响到循环终止条件，可能导致死循环
            continue
            print('玩')

    m+=1
