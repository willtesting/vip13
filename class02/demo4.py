# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 21:07
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：混合运算
"""
# 运算优先级：算术 > 比较 > 逻辑
y = 1 - 3 // 4 > 0 and 4 < 5
print(y)
y = 1 > 0 and 4 < 5
y = True and True
print(y)

# 编程规范：把实际意义的计算用括号括起来，从而决定运算优先级
# 这样，你自己不容易写错，别人也好理解
y = (1 - 3 // 4) > 0 and (4 < 5)
print(y)

