# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 21:57
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：for循环
"""
# 成员遍历（数据结构会再讲）
# s = 'abc'
# for ss in s:
#     print(ss)

# 一般来说，确定次数的，都可以用for
# 次数不固定的，可以用while，更灵活，是用break结束
# while可以自行随意改变控制条件的变化数


# 缩写为range(10)
# start，end，step
# 区间是左闭右开[start,end)，start<=i<end
# step是每次循环，i改变的值（i+=step）
# i的初始值是0
for i in range(0, 10, 1):
    print(i, end=',')
    # for循环里面改变i的值是不影响循环次数的
    i += 1

# 求100以内整数的和
print()
s = 0
for i in range(1, 101):
    s = s + i

print(s)
