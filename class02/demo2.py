# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 20:38
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：比较运算
"""
# 常见比较
x, y = 3, 4
print(x == y, x != y, x < y, x > y)

# Python里面比较可以连写
# 优先级从左到右，有括号先算括号里面
print(1 < x < y < 5)

# 字符串比较（等于，包含）
s1, s2 = 'ab', 'abc'
# 两个字符串必须字符和顺序都一模一样
print(s1 == s2)
# 必须有一个连续子串和s1一模一样，才叫s2包含s1
print(s2.__contains__(s1))

# 字符也有大小比较（实际上比较的是字符的ASCII码）
print(s1 > s2)

# 字符串算术运算（没有除法和减法）
# 两个字符串相加（叫拼接）
print(s1 + s2)
# 相乘（s1自己拼接自己3次）
print(s1 * 3)
