# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 21:21
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：分支语句：根据条件确定代码运行的逻辑
"""
# 今天下雨我就带伞，不下雨我就不带伞
s = input('今天下雨吗？')
if s:
    print('带伞')
else:
    # pass相当于空语句，这里啥也不做
    pass
    print('不带伞')

# 输入整数x,y
x, y = input('请输入整数x：'), input('请输入整数y：')
x, y = int(x), int(y)
# z = |x| + |y|
if x >= 0:
    if y >= 0:
        z = x + y
    else:
        z = x - y
else:
    if y >= 0:
        z = -x + y
    else:
        z = -x - y

print(z)

if x >= 0 and y >= 0:
    z = x + y
elif x < 0 and y >= 0:
    z = -x + y
elif x < 0 and y < 0:
    z = -x - y
else:
    z = x - y

print(z)
