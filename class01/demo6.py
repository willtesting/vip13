# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/21 20:16
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：input,exit说明
"""
# input输入的所有数据，最终都是接收为字符串
s = input("请输入字符串：")
print(s)

# 程序执行到这里就直接终止运行
exit('用户主动停止，运行完成')

# tpye(查看变量里面数据的类型)
print(type(s))



