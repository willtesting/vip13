# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/18 20:53
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：常用符号
"""
# 我假设：x，y都是1
x, y = 1, 1
# 等号：使某个未知数等于什么值（赋值）
x = 1
# 判断相等：==
print(x == 1)
# 括号，运算优先级
x = (2 + 3) * 5
# 双引号，单引号都表示字符串（Python里面没有字符的概念）
s = 'abc'
s = "a"
print(type(s))

# %：取余数
print(5 % 3)

# **：幂运算（次方）
print(5 ** 2)

# 赋值叠加
x = 2
# x = x + 1
x **= 2
print(x)

print(2 != 3)
