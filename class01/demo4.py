# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/18 21:23
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：数据类型
"""
# 整数：int
x = 1
print(type(x))
# 小数：float
y = 1.1
print(type(y))
# 布尔值：bool True or False
z = False
print(type(z))
# 字符串
s = "abc"
print(type(s))

# 互转
print(float(x))
print(int(y))
print(float(z))
# 我们把前3种统称为数字类型，他们可以随便互转
print(bool(1.1))

# 不为空，就是True
print(bool(''))
print(bool(s))

# 字符串转整数和小数：字符串本身符合整数或者小数时可以转，否则会报错
print(float('1.1'))

# 所有类型都可以转成字符串
print(str(1), str(1.1), str(False))
# 任何类型和字符串加（拼接）之前，都要转为字符串
print(str(1) + '1')

# 空类型（NoneType）：和所有类型都互斥
a = None
print(a)
# 可以转为bool，str
print(bool(a))

# bool值附加：False对应的情况
# 所有的空和零：None，''，0，0.0，[]，()，{}
print(bool(None), bool(''), bool(0), bool(0.0), bool([]), bool(()), bool({}))
