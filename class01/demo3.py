# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/18 21:03
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：基础语法二
"""

# 行：一行
# 多行可以用分号写在一行（但是不推荐）
x = 1; y = 2

# 多行构成一个语句
s = 'fdsfnsdajfklsdjaklfjasdlkfjsdklfjkldsajfkladsjfklasdjfklsdajafkldas' \
    'jflkdsjafklasdfjklsadjfklsdjfkldsajfklasjdfkljsdklfjadslkfja'
z = x + \
    y
print(z)

# 这是单行注释

s = '''
严格来说，Python只有单行注释
多行注释这么表示？
我们可以用三个单引号或者三个双引号
'''
print(type(s))
