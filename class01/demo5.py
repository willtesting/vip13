# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/18 21:46
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：面向过程的编程实例
"""
# 设score=0
score = 0
# A 跑完一圈
score += 34
# AB接棒
score += 2
# B 跑完一圈
score += 30
# BC接棒
score += 2
# C 跑完
score += 38

# 最终时间
print(score)





