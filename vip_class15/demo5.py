# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/21 22:08
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：异常处理
"""
import traceback

try:
    # 输入两个数
    x, y = input('x:'), input('y:')
    # 输出他们的和
    z = float(x) + float(y)
    print(z)
except Exception as e:
    # 当上面的代码执行报错，就执行这里面的
    # 你在上述代码报错的时候，要给出的提示，或者处理方案
    # 这里面最好保证代码不再报错
    # int(x)
    print('请输入数字')

    # 获取报错信息
    # 简单的提示信息
    print(e.__str__())
    # 完整的堆栈信息
    print(traceback.format_exc())

finally:
    # 无论上面的代码是否报错，都会执行finally
    # 作为后置处理
    print('执行完成')
