# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/12 20:42
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : web关键字驱动的封装
import re
import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select

from vip_class11.verify import Verify
from vip_class11.zsq import relations
from vip_class16.common.Logger import logger


class Web:
    """web自动化关键字类"""

    def __init__(self, path):
        self.driver: webdriver.Chrome = None
        # 使用字典保存关联的数据
        self.relation_dict = {}
        self.path = path

    def openbrowser(self, browser=''):
        """
        打开浏览器
        :param br: 默认使用谷歌浏览器
        :param browser: 浏览器类型：支持谷歌/火狐....
        :return:
        """
        if browser == 'ff':
            self.br = 'ff'
            self.driver = webdriver.Firefox()
        elif browser == 'ie':
            self.br = 'ie'
            self.driver = webdriver.Ie()
        elif browser == 'eg':
            self.br = 'eg'
            self.driver = webdriver.Edge()
        else:
            option = Options()
            # 去掉自动化标识
            option.add_experimental_option('excludeSwitches', ['enable-automation'])
            option.add_argument('--disable-blink-features=AutomationControlled')

            # 关掉密码弹窗
            prefs = {}
            prefs['credentials_enable_service'] = False
            prefs['profile.password_manager_enabled'] = False
            option.add_experimental_option('prefs', prefs)

            self.driver = webdriver.Chrome(options=option)

        # 添加隐式等待
        self.driver.implicitly_wait(5)
        # 最大化浏览器
        self.driver.maximize_window()

    def geturl(self, url=''):
        """
        打开被测地址
        :param url: 被测地址，支持完整写法或者仅域名或者IP的写法
        :return:
        """
        if url.startswith('http'):
            self.driver.get(url)
        else:
            self.driver.get('http://' + url)

    def __find_element(self, locator: str = '') -> WebElement:
        """
        统一定位方式
        :param locator: 元素定位器，同时支持xpath/css/id
        :return: 定位到的元素，如果没有就返回None
        """
        if locator is None or locator == '':
            return None
        # xpath通常以'/'开头，可能也会以'('开头:
        elif locator.startswith('/') or locator.startswith('('):
            element = self.driver.find_element('xpath', locator)
        elif locator.startswith('#') or locator.__contains__('>'):
            element = self.driver.find_element('css selector', locator)
        else:
            element = self.driver.find_element('id', locator)

        # 给操作的元素高亮显示
        if element:
            self.driver.execute_script("arguments[0].style.background = '#00ff50'", element)
        return element

    @relations
    def input(self, locator='', value=''):
        """
        文本输入
        :param locator: 元素定位
        :param value: 输入的内容
        :return:
        """
        element = self.__find_element(locator)
        if value.endswith('.png') or value.endswith('.jpg'):
            value = self.path + '\\lib\\' + value
        element.send_keys(value)

    def clear(self, locator):
        """清空输入框"""
        element = self.__find_element(locator)
        element.clear()

    def click(self, locator):
        """
        点击元素
        :param locator:
        :return:
        """
        element = self.__find_element(locator)
        element.click()

    def jsclick(self, locator=''):
        """js点击"""
        element = self.__find_element(locator)
        self.driver.execute_script("arguments[0].click()", element)

    def tryclicks(self, locator=''):
        """点击"""
        try:
            self.driver.implicitly_wait(3)
            element = self.__find_element(locator)
            element.click()
        except Exception as e:
            print('尝试点击元素 %s 错误' % locator)
        finally:
            self.driver.implicitly_wait(10)

    def myslide(self, lo=''):
        ele = self.__find_element(lo)

        # 要操作鼠标就要使用selenium 的 actionChains类
        action = ActionChains(self.driver)
        # 鼠标按住滑块
        action.click_and_hold(ele)
        # 滑动的距离：300像素
        action.move_by_offset(300, 0)
        # 松开鼠标
        action.release()
        # 一定要记得加perform()，不然不生效！！！
        action.perform()

    def gettext(self, locator='', reg=''):
        """
        获取元素的文本
        :param locator:
        :param reg: 对文本进行正则处理
        :return:
        """
        element = self.__find_element(locator)
        text = element.text
        # 如果有正则，就按正则来处理
        if reg:
            text = re.findall(reg, text)
            if text:
                text = text[0]

        # 如果获取的属性可以是一个系统变量，则用{text}保存起来
        self.relation_dict['text'] = text
        return text

    @relations
    def saveparams(self, name='', value=''):
        """
        把参数保存为你需要的名字
        :param name: 参数名
        :param value: 参数值
        :return:
        """
        self.relation_dict[name] = value

    def sleep(self, t='1'):
        """固定等待"""
        t = float(t)
        time.sleep(t)

    def intoiframe(self, lo=''):
        """切入iframe"""
        ele = self.__find_element(lo)
        self.driver.switch_to.frame(ele)

    def select(self, lo='', value=''):
        """下拉框选择"""
        ele = self.__find_element(lo)
        se = Select(ele)
        se.select_by_visible_text(value)

    def getverify(self, lo):
        """获取图文验证码"""
        # 截取验证码图片
        ele_img = self.__find_element(lo)
        ele_img.screenshot(self.path + 'lib/verify.png')

        verify = Verify('wuqingfqng', 'wuqing&fqng', '904357')
        # 调用方法，获取验证码
        ver = verify.get_verify(self.path + 'lib/verify.png')
        self.relation_dict['verify'] = ver

    @relations
    def assertequal(self, actual='', expect=''):
        """断言相等的关键字"""
        logger.info(actual)
        logger.info(expect)
        if actual == expect:
            return '断言成功'
        else:
            return False, '断言失败'

    def quit(self):
        self.driver.quit()
        self.driver = None

