# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/21 21:05
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：反射
"""
from vip_class15.keywords.webkeys import Web

step = {'name': '打开浏览器',
        'method': 'openbrowser',
        'browser': 'gc',
        }
# 把字典的值转成列表
values = list(step.values())
print(values)
# 参数
params = values[2:]
print(params)

# 反射
web = Web()

# # 假设通过某种方式，可以获取对象的函数
# func = web.openbrowser
# func(*params)

# 反射，反向从对象里面获取和字符串一样的函数或者变量
method = step.get('method')
func = getattr(web, method)
print(func.__name__)
func(*params)