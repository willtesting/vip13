# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/21 20:26
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：yaml读写
pip install pyyaml
"""
import yaml

my_data = {
    'a': ['1', 2, 3],
    'b': '333',
    "c": True
}

# # 打开一个文件
# f = open('./lib/demo.yml', mode='w')
# # 写入
# f.write(str(my_data))
# # 关闭保存
# f.close()


# 打开一个文件
# mode='w'，覆盖写入；mode='a'，追加写入
f = open('./lib/demo.yml', mode='w', encoding='utf8')
# 使用yaml写入
yaml.safe_dump(my_data, f)
f.close()

# 读
f = open('./lib/login.yml', mode='r', encoding='utf8')
new_data = yaml.safe_load(f)
f.close()
print(new_data)
