# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/21 20:43
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：yaml数据驱动
"""
import yaml

from vip_class15.keywords.webkeys import Web


def run_step_old(step: dict, obj: Web):
    """选择关键字执行"""
    # 获取到关键字
    method = step.get('method')

    # 根据步骤里面关键字的字符串，调用指定的函数
    if method == 'openbrowser':
        # 调相应函数，并且传参
        res = obj.openbrowser(step.get('browser'))
        return res

    if method == 'geturl':
        res = obj.geturl(step.get('url'))
        return res

    if method == 'input':
        res = obj.input(step.get('locator'),step.get('value'))
        return res

    if method == 'click':
        res = obj.click(step.get('locator'))
        return res

    if method == 'sleep':
        res = obj.sleep(step.get('t'))
        return res


def run_step_new(step: dict, obj: Web):
    """反射执行"""
    # 把字典的值转成列表
    values = list(step.values())
    # 参数
    params = values[2:]
    # print(params)

    # 反射，获取关键字，并调用执行
    method = step.get('method')
    func = getattr(web, method)
    # print(func.__name__)
    func(*params)


# 创建关键字对象
web = Web()

# 读
f = open('./lib/login.yml', mode='r', encoding='utf8')
new_data = yaml.safe_load(f)
f.close()
# print(new_data)

for key in new_data.keys():
    # 遍历key：一个page的所有用例
    # print(key)
    # 获取所有用例
    cases_data = new_data.get(key)
    # 遍历用例
    for cases in cases_data:
        # print(cases)
        # 获取所有步骤
        steps = cases.get('cases')
        for step in steps:
            print(step)
            run_step_new(step,web)
