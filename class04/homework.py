# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/28 20:01
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# import time
#
# height = [i for i in range(5000, 0, -1)]
# # print(height)
#
# print('降序排升序')
# t1 = time.time()
# j = 0
# while j < len(height) - 1:
#     # 第一轮
#     i = 0
#     # -j的意思是，每一轮都少比一个元素
#     while i < len(height) - 1 - j:
#         if height[i] > height[i + 1]:
#             # 交换
#             height[i], height[i + 1] = height[i + 1], height[i]
#
#         i += 1
#
#     j += 1
#
# # print(height)
#
# # print(height)
# t2 = time.time()
# print(t2 - t1)
#
# # 升序排升序
# print('升序排升序')
# t1 = time.time()
# j = 0
# while j < len(height) - 1:
#     # 第一轮
#     i = 0
#     # -j的意思是，每一轮都少比一个元素
#     while i < len(height) - 1 - j:
#         if height[i] < height[i + 1]:
#             # 交换
#             height[i], height[i + 1] = height[i + 1], height[i]
#
#         i += 1
#
#     j += 1
#
#
# # print(height)
#
# t2 = time.time()
# print(t2 - t1)

n = 3
s = 'will,roy,tufei,roy,will,tufeiroywilltufeiroywill'
# # 先把n个都替换
# s = s.replace('will','kaka',n)
# # 再把前n-1替换回去
# s = s.replace('kaka','will',n-1)
# print(s)

# # 前n个will替换成roy？
# # 解决冲突，最好是使用标志性字符串
# # 先把n个都替换
# s = s.replace('will', '$roy', n)
# # 再把前n-1替换回去
# s = s.replace('$roy', 'will', n - 1)
# s = s.replace('$roy', 'roy')
# print(s)

import re

s = '订单号：  202207192221383736    付款金额（元）：  456.00 元'
# 订单号是18位连续数字
res = re.findall(r'\d{18}', s)
print(res[0])

# 金额：至少1位数字（可以为0），然后是小数点，后面跟两位数字
# 小数点，在正则里面有含义，我们用\.把它转成普通的小数点
res = re.findall(r'\d+\.\d{2}', s)
print(res[0])
