# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/25 21:40
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：选择排序
"""
# height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]
import time

height = [i for i in range(10000, 0, -1)]

t1 = time.time()
j = 0
while j < len(height) - 1:
    # 第一个不用比
    i = 1
    # 记录最大的下标
    max = 0
    while i < len(height) - j:
        # 两两比较。记录较大值的下标
        if height[i] > height[max]:
            max = i

        i += 1

    # 把最大的交换到末尾
    height[max], height[len(height) - 1 - j] = height[len(height) - 1 - j], height[max]
    # print(height)
    j += 1

# print(height)
t2 = time.time()
print(t2 - t1)
