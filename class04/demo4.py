# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/25 20:43
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import json

html = \
    '''<html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta content="always" name="referrer"><meta name="theme-color" content="#ffffff"><meta name="description" content="全球领先的中文搜索引擎、致力于让网民更便捷地获取信息，找到所求。百度超过千亿的中文网页数据库，可以瞬间找到相关的搜索结果。"><link rel="shortcut icon" href="
        /favicon.ico
        " type="image/x-icon" /><link rel="search" type="application/opensearchdescription+xml" href="/content-search.xml
        " title="百度搜索" /><link rel="icon" sizes="any" maskhref="//www.baidu.com/img/baidu_85beaf5496f291521eb75ba38eacbd87.svg
        "><link rel="dns-prefetch" href="//dss0.bdstatic.com
        "/><link rel="dns-prefetch" href="//dss1.bdstatic.com
        "/><link rel="dns-prefetch" href="//ss1.bdstatic.com
        "/><link rel="dns-prefetch" href="//sp0.baidu.com
        "/><link rel="dns-prefetch" href="//sp1.baidu.com
        "/><link rel="dns-prefetch" href="//sp2.baidu.com
        "/><title>百度一下，你就知道</title><style index="newi" type="text/css">
        <body>
        <div class="c-tips-container" id="c-tips-container"></div>
        <script>
        var s_session={
            "logId":"3614294667",
            "seqId":"3614294907",
            "sessionId":"",
            "debug":false,
            "userTips":"{}",
            "curmod":"2",
            "firstmod":"",
            "logoCode":false,
            "isFesLogo":false,
            "sid":"36454_34813_36422_36166_36488_36055_36419_26350_36299_36468_36315_36447",
            "mSid":"",
            "sample_value":"",
            "isLogin":false,
            "agingVoice": "",
            "卡卡": "632232258",
            "Will": "1052949192",
            "Phone": "18874922908"
        };
        window.__async_strategy = 2;
        </script>
        </body>
        </html>'''

print(html)

import re

# r''字符串里面的写法忽略所有转义
res = re.findall(r'<title>(.*)</title>', html)
print(res)

res = re.findall(r'<script>(.*)</script>', html, re.S)
res = re.sub(r'\s{2,}', '', res[0])
print(res)

res = re.findall(r'({.*});', html, re.S)
print(res)
if res:
    res = json.loads(res[0])
    print(res)

# res = re.findall(r'\d{7}', html)
# print(res)
#
# # QQ号：第一位不为0，至少有6位，最多12位
# res = re.findall(r'[1-9]\d{5,11}', html)
# print(res)
#
# # 手机号：11位，第一个数字一定是1，第二位3-9，其他的都是数字
# res = re.findall(r'1[3-9]\d{9}|[1-9]\d{3}-\d[6]', html)
# print(res)
#
# # 贪心，会一直往后找
# res = re.findall(r'<link .*/>', html,re.S)
# print(res)
#
# # 非贪心，匹配到第一个/>后停止
# res = re.findall(r'<link .*?/>', html,re.S)
# print(res)






