# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/25 21:22
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：冒泡排序
"""
# # 平时要排序，直接对列表sort
# height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]
# height.sort()
# print(height)

# 排序：第一个面试
# 思维方法，效率比较
height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]

j = 0
while j < len(height) - 1:
    # 第一轮
    i = 0
    # -j的意思是，每一轮都少比一个元素
    while i < len(height) - 1 - j:
        if height[i] > height[i + 1]:
            # 交换
            height[i], height[i + 1] = height[i + 1], height[i]

        i += 1

    j += 1

    print(height)

# 交换，其实就是重新赋值
a, b = 1, 2
a, b = b, a
print(a, b)
