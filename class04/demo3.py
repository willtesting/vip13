# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/25 20:26
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字符串的替换，反转，截取
"""

s = 'willroytufeiroywilltufeiroywilltufeiroywill'
# replace替换：得到一个新的字符串
# old：需要被替换的子串（如果没有，就不会替换）
# new：替换成新的子串
# count：替换的次数，默认是-1，也就是全部替换
s = s.replace('will', 'kaka', -1)
print(s)

# 替换最后一个怎么办？
# 字符串可以看作一个字符列表
# 反转
s = s[::-1]
s= s.replace('kaka'[::-1],'will'[::-1],1)
s = s[::-1]
print(s)

# 怎么替换第n个子串？

# json字符串处理为字典
json_s = '[{"status":401,"msg":"用户名密码错误","st":true}]'
print(json_s)
# 处理成字典？
import json
json_dict = json.loads(json_s)
print(json_dict)

# 列表/字典转json字符串
json_dict = {'status': 401, 'msg': '用户名密码错误', 'st': True}
json_s = json.dumps(json_dict)
print(json_s)
