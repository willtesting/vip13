# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/25 20:05
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：列表去重
"""
mylist = [1, 1, 2, 3, 4, 4, 5, 'a', 'a']
# 先把列表转成集合，这样是不是就实现了去重的效果。
myset = set(mylist)
# 再转为列表
mylist = list(myset)
print(mylist)
