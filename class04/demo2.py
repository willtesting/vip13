# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/25 20:11
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字符串处理
"""
params = 'username=Will&password=111111&a=c'
# 分割：以&为节点，分割所有键值对
params = params.split('&')
print(params)

params_dict = {}
# 每一个元素都按=号切割为键和值
for p in params:
    key, value = p.split('=')
    print(key, value)
    params_dict[key] = value

print(params_dict)

params_dict = {'username': 'Will', 'password': '111111', 'a': 'c'}
params = []
for item in params_dict.items():
    # print(item)
    s = '='.join(item)
    params.append(s)

print(params)
params = '&'.join(params)
print(params)
