# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/11 22:04
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：jsonpath示例
"""
import json

import jsonpath

res = '{"Srcid":"5809","ResultCode":"0","status":"0","QueryID":"2593572719","Result":[{"DisplayData":{"strategy":{"tempName":"ip","precharge":"0","ctplOrPhp":"1"},"resultData":{"tplData":{"srcid":"5809","resourceid":"5809","OriginQuery":"113.246.108.37","origipquery":"113.246.108.37","query":"113.246.108.37","origip":"113.246.108.37","location":"\u6e56\u5357\u7701\u957f\u6c99\u5e02\u5cb3\u9e93 \u7535\u4fe1","userip":"","showlamp":"1","tplt":"ip","titlecont":"IP\u5730\u5740\u67e5\u8be2","realurl":"http:\/\/www.ip138.com\/","showLikeShare":"1","shareImage":"1","data_source":"AE"},"extData":{"tplt":"ip","resourceid":"5809","OriginQuery":"113.246.108.37"}}},"ResultURL":"http:\/\/www.ip138.com\/","Weight":"2","Sort":"1","SrcID":"5809","ClickNeed":"0","SubResult":[],"SubResNum":"0","ar_passthrough":[],"RecoverCacheTime":"0"}],"data":[{"srcid":"5809","resourceid":"5809","OriginQuery":"113.246.108.37","origipquery":"113.246.108.37","query":"113.246.108.37","origip":"113.246.108.37","location":"\u6e56\u5357\u7701\u957f\u6c99\u5e02\u5cb3\u9e93 \u7535\u4fe1","userip":"","showlamp":"1","tplt":"ip","titlecont":"IP\u5730\u5740\u67e5\u8be2","realurl":"http:\/\/www.ip138.com\/","showLikeShare":"1","shareImage":"1"}],"ResultNum":"1"}'
res = json.loads(res)

# 最外层的，可以直接写键名
lo = jsonpath.jsonpath(res,'status')
# 你获得的结果存在一个列表里面，一般来说，我们要取第一个
print(lo)

lo = jsonpath.jsonpath(res,'status1')
# 如果jsonpath匹配没有结果，返回False
print(lo)

# 路径写法
lo = jsonpath.jsonpath(res,'$.data[0].location')
print(lo)


