# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/9 20:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：session管理
"""
import requests


# session = requests.session()    # 创建一个会话
# res = session.post('http://testingedu.com.cn:8081/inter/HTTP/auth')
# print(res.text)
# token = res.json().get('token')
# session.headers['token'] = token
#
# res = session.post('http://testingedu.com.cn:8081/inter/HTTP//register',
#                     data={'username': 'Will007','pwd': '123456','nickname': '测试账号','describe': '测试组长'})
# print(res.text)
#
# res = session.post('http://testingedu.com.cn:8081/inter/HTTP/login',
#                     data={"username":'Will007','password':'123456'})
# print(res.text)
#
# res = session.post('http://testingedu.com.cn:8081/inter/HTTP/auth')
# print(res.text)

# 电商登录
session = requests.session()    # 创建一个会话
res = session.post('http://testingedu.com.cn:8000/index.php?m=Home&c=User&a=do_login&t=0.8799443162598024',
                    data={'username': '13800138006','password': '123456','verify_code': '1'})
# print(res.json())
print(res.cookies)

# 访问个人信息
res = session.get('http://testingedu.com.cn:8000/Home/User/info.html')
# print(res.text)
print(res.cookies)


