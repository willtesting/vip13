# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/21 21:32
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：Excel数据驱动
"""
import os
import traceback

from vip_class15.common.Excel import Reader
from vip_class21.interkeys import Inter

path = os.path.abspath(__file__)
print(path)
proname = 'vip13'
# 通过项目名字，截取到绝对路径
path = path[:path.rfind(proname) + len(proname)]
print(path)


def run_step_new(step: list, obj: Inter):
    """反射执行"""
    # step = ['', '', '打开浏览器', 'openbrowser', '', '', '', '', '']

    if len(step[0]) > 0 or len(step[1]) > 0:
        # 分组信息，不跑
        print(step)
        return

    # 获取参数
    params = step[4:7]
    # 截取不需要的参数：保留列表里面不为空的参数
    params = params[:params.index('')]
    print(params)
    # 反射，获取关键字，并调用执行
    method = step[3]
    func = getattr(obj, method)
    func(*params)


# 创建关键字对象
web = Inter()

# 读取用例
reader = Reader()
# 打开一个excel
reader.open_excel('./lib/HTTP接口用例.xlsx')
# 获取所有sheet
sheets = reader.get_sheets()
for sheet in sheets:
    # 设置读取的sheet页面
    reader.set_sheet(sheet)
    # 读取当前sheet的所有行
    lines = reader.readline()
    # 遍历每一行，执行
    for line in lines:
        # 运行可能报错，我做一个异常处理
        # 还是会全  部跑完
        # 会把报错打出来
        # 统一处理所有报错

        # 重试
        for i in range(3):
            try:
                run_step_new(line, web)
                break
            except:
                print(traceback.format_exc())

