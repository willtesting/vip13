# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/9 21:27
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：接口关键字库
"""
import json
import traceback

import jsonpath
import requests

from vip_class11.zsq import relations
from vip_class16.common.Logger import logger


class Inter:

    def __init__(self):
        self.session = requests.session()
        self.url = 'http://testingedu.com.cn:8081/inter/HTTP/'

        # 关联
        self.relation_dict = {}
        # 保存结果
        self.res = ''
        self.json_res = {}

    def seturl(self, url=''):
        """设置url前缀"""
        if not url.endswith('/'):
            self.url = url + '/'
        else:
            self.url = url

        return self.url

    @relations
    def addheader(self, key='', value=''):
        self.session.headers[key] = value
        return str(self.session.headers)

    def removeheader(self, key=''):
        """删除一个或者多个键"""
        keys = key.split(',')
        for key in keys:
            try:
                self.session.headers.pop(key)
            except Exception as e:
                logger.warning(f'请求头里面没有这个键{key}')
                logger.exception(e)

        return str(self.session.headers)

    def reauth(self):
        """注销登录-重新授权"""
        res = self.session.post(self.url + 'logout')
        logger.debug(res.text)
        res = self.session.post(self.url + 'auth')
        logger.debug(res.text)
        token = res.json().get('token')
        self.session.headers['token'] = token

    def get(self, url='', params=''):
        """
        get请求关键字，直接在url后面拼参数
        """
        # 支持同时传相对路径和绝对路径
        if not url.startswith('http'):
            url = self.url + url

        # 直接在url后面拼参数
        url = url + '?' + params
        self.res = self.session.get(url)
        logger.info(self.res.text)
        # json 解析
        try:
            res = self.res.text
            res = res[res.find('{'):res.rfind('}') + 1]
            self.json_res = json.loads(res)
            return str(self.json_res)
        except:
            self.json_res = {'msg': traceback.format_exc()}
            logger.info(self.json_res)
            return self.res.text

    @relations
    def post(self, url='', params=''):
        """发送post请求"""
        # 支持同时传相对路径和绝对路径
        if not url.startswith('http'):
            url = self.url + url

        json_parmas = None
        data_params = None

        # 兼容json
        if params and params.startswith('{'):
            json_parmas = json.loads(params)
        else:
            # 参数转字典
            data_params = self.parmas_to_data(params)

        self.res = self.session.post(url, data=data_params, json=json_parmas)
        logger.info(self.res.text)
        # json 解析
        try:
            res = self.res.text
            res = res[res.find('{'):res.rfind('}') + 1]
            self.json_res = json.loads(res)
            return str(self.json_res)
        except:
            self.json_res = {'msg': traceback.format_exc()}
            logger.info(self.json_res)
            return self.res.text

    def parmas_to_data(self, parmas=''):
        """url格式参数转字典"""
        parmas_list = parmas.split('&')
        parmas_dict = {}
        for p in parmas_list:
            key_value = p.split('=')
            if len(key_value) > 1:
                parmas_dict[key_value[0]] = key_value[1]
            else:
                parmas_dict[key_value[0]] = None

        return parmas_dict

    def savejson(self, key='', paramname=''):
        """
        从上一个请求的结果里面，取key的值，保存到关联字典里面，参数名叫paramname
        :param key: 需要取值的键
        :param paramname: 保存后参数名字
        """
        # 比如只传了key，那就是说保存为同样参数名
        if not paramname:
            keys = key.split(',')
            for key in keys:
                value = jsonpath.jsonpath(self.json_res, key)
                if value:
                    value = str(value[0])
                else:
                    value = str(value)
                self.relation_dict[key] = value
        else:
            keys = key.split(',')
            paramnames = paramname.split(',')
            for i in range(len(keys)):
                value = jsonpath.jsonpath(self.json_res, key)
                if value:
                    value = str(value[0])
                else:
                    value = str(value)
                self.relation_dict[paramnames[i]] = value

        return str(self.relation_dict)

    @relations
    def assertequals(self, key='', value=''):
        """
        从上一步结果里面判断一个键的值等于你给定的值
        :param key: json结果的键
        :param value: 期望值
        """
        keys = key.split(',')
        for key in keys:
            act_value = jsonpath.jsonpath(self.json_res,key)
            if act_value:
                act_value = str(act_value[0])
            else:
                act_value = str(act_value)

            if act_value != value:
                logger.error('断言失败')
                logger.error(f"断言的键：{key}，实际结果：{act_value} VS 期望值：{value}")
                return False, f"断言失败！断言的键：{key}，实际结果：{act_value} VS 期望值：{value}"

        logger.info('断言成功')
        return True

    @relations
    def assertcontains(self, key='', value=''):
        """
        从上一步结果里面判断一个键的值包含你给定的值
        :param key: json结果的键
        :param value: 期望值
        """
        keys = key.split(',')
        for key in keys:
            act_value = jsonpath.jsonpath(self.json_res,key)
            if act_value:
                act_value = str(act_value[0])
            else:
                act_value = str(act_value)

            if not act_value.__contains__(value):
                logger.error('断言失败')
                logger.error(f"断言的键：{key}，实际结果：{act_value} VS 期望值：{value}")
                return False, f"断言失败！断言的键：{key}，实际结果：{act_value} VS 期望值：{value}"

        logger.info('断言成功')
        return True
