# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/9 20:15
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：接口可行性分析
"""
from interkeys import Inter

inter = Inter()
inter.seturl('http://testingedu.com.cn:8081/inter/HTTP/')

inter.reauth()
inter.post('login','username=Will&password=123456')

inter.reauth()

inter.post('login','username=Will&password=1234567')

inter.reauth()
