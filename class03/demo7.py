# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 21:59
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字典的遍历
"""
my_dict = {'name': 'will', 'age': 18, 'height': '170cm'}
print(my_dict)

keys = my_dict.keys()
keys = list(keys)
print(keys)

# 键的遍历
for key in my_dict.keys():
    print(key, ':', my_dict.get(key))

# 遍历值（不能获得键）
for value in my_dict.values():
    print(value)

# 键值对遍历
for item in my_dict.items():
    print(item)

# 键值同时遍历
for key, value in my_dict.items():
    print(key, value)


print(my_dict)
# 清除所有键值对（列表也可以clear）
my_dict.clear()
print(my_dict)
