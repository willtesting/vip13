# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 21:36
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：元组
"""

# 定义：关键字tuple
my_tup = ()
print(type(my_tup))
my_tup = (1,)
print(type(my_tup))
my_tup = (1, 22, 3)
print(my_tup)

# 列表和元组可以互转
my_list = list(my_tup)
print(my_list)
print(tuple(my_list))
