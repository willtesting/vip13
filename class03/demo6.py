# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 21:43
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字典
"""
# 定义，关键字：dict
my_dict = {}
print(type(my_dict))
my_dict1 = {'age': 1, 2: 3}
# 一般来说，我们要求，键是字符串，因为键是有意义的
my_dict = {'name': 'will', 'age': 18, 'height': '170cm'}
print(my_dict)

# 读，推荐使用第一种，当键不存在，第一种返回None，第二种是报错
print(my_dict.get('name'))
print(my_dict['name'])

# 键存在更新/键不存在新增
my_dict['name'] = '老Will'
print(my_dict)

my_dict['nickname'] = '老Will'
print(my_dict)

# 删除键：键不存在是会报错的，且不能通过值删除
# my_dict.pop('nickname')
del my_dict['nickname']
print(my_dict)

# 把一个字典，更新到另一个字典里面，键存在更新/键不存在新增
my_dict.update(my_dict1)
print(my_dict)


