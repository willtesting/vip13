# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 20:27
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：变量
"""

print(id('1'))

# 定义
a = None
print(id(a))

a = 1
b = 1
print(id(a))
print(id(b))

a = 1
print(a)
a = '2'
print(a)

name = 'will'
name_1 = 'will'

your_name = 'will'


