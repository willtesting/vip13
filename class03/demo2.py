# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 20:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：列表
"""
# 定义
my_list_1 = []
print(bool(my_list_1))
my_list_1 = [1, 2, '3']
print(my_list_1)
# for循环简写定义
my_list_3 = [i for i in range(10)]
print(my_list_3)
# # 等同于
# my_list_3 = []
# for i in range(10):
#     my_list_3.append(i)
#
# print(my_list_3)

# 获取值
print(my_list_3[3])
# 改变值
my_list_3[3] = 33
print(my_list_3)

# 末尾新增
my_list_3.append('阿')
my_list_3.append('阿')
print(my_list_3)
# 指定位置（下标）加
my_list_3.insert(3,'333')
print(my_list_3)

# 按下标删
my_list_3.pop(3)
del my_list_3[3]
print(my_list_3)

# 按值删：删第一个找到值
my_list_3.remove('阿')
print(my_list_3)