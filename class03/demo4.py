# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 21:26
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：列表的遍历
"""
# 求1~10以内整数的和
my_list = [i for i in range(1, 10)]
print(my_list)

# 成员遍历（只读）
s = 0
for i in my_list:
    s += i
    # print(i)

print(s)

s = 0
print(len(my_list))
# 下标遍历（读写）
for i in range(len(my_list)):
    s += my_list[i]

print(s)
# 把列表里面每一个元素的值加1
for i in range(len(my_list)):
    my_list[i] += 1

print(my_list)


