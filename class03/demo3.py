# -*- coding: utf-8 -*-
"""
@Time ： 2022/11/23 21:08
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：列表的特殊操作
"""
# 定义
my_list_3 = [i for i in range(10)]
print(my_list_3)

print(len(my_list_3))
# 下标越界
# print(my_list_3[10])

# 列表的拼接，得到一个新的列表
my_list_3 = my_list_3 + ['a', 'b', 'c']
print(my_list_3)

# # 列表截取（按下标，截取得到一个新的列表）
# my_list = my_list_3[1:9]
# print(my_list)

# my_list_3[start:end:step]
# start：默认是0，代表开始下标
# end：默认是len(my_list_3)，代表列表的末尾
# step：默认是1，代表正向一个一个截取，负数代表反向
# 截取的区间是左闭右开start<=i<end
# print(my_list_3[:8])
# print(my_list_3[3:])
# print(my_list_3[1:8:2])

# 下标为负数的时候，其实是反向下标，从-1
print(my_list_3[-2])

# 列表的反向
# step为负数：start不填代表len(my_list_3)-1，
# end不填代表下标的起始位置（反转的时候不能填-1,可以填None）
print(my_list_3[::-1])
print(my_list_3[12:None:-1])

