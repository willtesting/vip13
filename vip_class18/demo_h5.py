# -*- coding: utf-8 -*-
"""
@Time ： 2023/1/4 20:17
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
import time

from appium import webdriver

caps = {}
caps["platformName"] = "Android"
caps["deviceName"] = "127.0.0.1:7555"
caps["appPackage"] = "com.android.browser"
caps["appActivity"] = ".BrowserActivity"
caps["noReset"] = "true"
caps["ensureWebviewsHavePages"] = True

driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)

# 隐式等待
driver.implicitly_wait(10)

el1 = driver.find_element('id',"com.android.browser:id/url")
el1.click()
el1.send_keys("m.baidu.com")

# 输入回车键
driver.keyevent(66)


els4 = driver.find_element('xpath',"//*[@resource-id=\"index-kw\"]")
els4.send_keys("阳了")
els5 = driver.find_element('xpath',"//*[@resource-id=\"index-bn\"]")
els5.click()

time.sleep(3)
driver.quit()