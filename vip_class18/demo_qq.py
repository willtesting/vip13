# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
import time

from appium import webdriver

caps = {}
caps["platformName"] = "Android"
caps["deviceName"] = "127.0.0.1:7555"
caps["appPackage"] = "com.tencent.mobileqq"
caps["appActivity"] = ".activity.SplashActivity"
caps["noReset"] = "true"
caps["ensureWebviewsHavePages"] = True

driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
driver.implicitly_wait(10)

el1 = driver.find_element('accessibility id',"请输入QQ号码或手机或邮箱")
el1.send_keys("3599292078")
el2 = driver.find_element('id',"com.tencent.mobileqq:id/qqp")
el2.click()
el3 = driver.find_element('accessibility id',"登 录")
el3.click()

time.sleep(5)

el5 = driver.find_element('accessibility id',"帐户及设置")
el5.click()
el6 = driver.find_element('accessibility id',"设置")
el6.click()
el7 = driver.find_element('accessibility id',"帐号管理")
el7.click()
el8 = driver.find_element('accessibility id',"退出")
el8.click()
el9 = driver.find_element('xpath','//*[@text=\"退出登录\"]')
el9.click()
el10 = driver.find_element('id',"com.tencent.mobileqq:id/dialogRightBtn")
el10.click()

time.sleep(3)

driver.quit()