# -*- coding: utf-8 -*-
"""
@Time ： 2022/7/28 20:44
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：图文验证码破解
笔记文档：https://www.yuque.com/docs/share/73a7c983-4fcd-4bcf-8838-de0675b1738a
项目路径：http://47.105.110.138:8000/index.php/Home/user/login.html
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

# 创建浏览器对象
from vip_class11.verify import Verify

driver = webdriver.Chrome()
# 最大化
driver.maximize_window()
# 添加隐式等待
driver.implicitly_wait(5)
# 打开网站
driver.get('http://47.105.110.138:8000/index.php/Home/user/login.html')

# id定位
ele = driver.find_element(By.ID, 'username')
# 操作元素
ele.send_keys('13800138006')
driver.find_element('xpath', '//*[@id="password"]').send_keys('123456')

# 截取验证码图片
ele_img = driver.find_element('xpath','//*[@id="verify_code_img"]')
ele_img.screenshot('./verify.png')

verify = Verify('wuqingfqng', 'wuqing&fqng', '904357')
# 调用方法，获取验证码
ver = verify.get_verify('./verify.png')
print(ver)

driver.find_element('xpath', '//*[@id="verify_code"]').send_keys(ver)

time.sleep(4)
driver.find_element('partial link text', '登').click()



