# -*- coding: utf-8 -*-
"""
@Time ： 2022/8/2 21:33
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：12306滑块验证码破解
"""
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options

option = Options()
# 在打开浏览器之前，去掉自动化标识
option.add_experimental_option('excludeSwitches', ['enable-automation'])
option.add_argument('--disable-blink-features=AutomationControlled')

# 关掉密码弹窗
prefs = {}
prefs['credentials_enable_service'] = False
prefs['profile.password_manager_enabled'] = False
option.add_experimental_option('prefs', prefs)

driver = webdriver.Chrome(options=option)
driver.maximize_window()
driver.implicitly_wait(10)

# 输入信息
driver.get('https://kyfw.12306.cn/otn/resources/login.html')
driver.find_element('xpath','//*[@id="J-userName"]').send_keys('i_wanting@yeah.net')
driver.find_element('xpath','//*[@id="J-password"]').send_keys('xiaobao168')
driver.find_element('xpath','//*[@id="J-login"]').click()

# 找到滑块
ele = driver.find_element('xpath','//*[@id="nc_1_n1z"]')

# 使用selenium鼠标操作类
action = ActionChains(driver)
# 按住滑块
action.click_and_hold(ele)
# 滑动到右边300像素位置
action.move_by_offset(300,0)
# 松开鼠标
action.release()
# 使所有操作生效!!!!!!!!!!
action.perform()
