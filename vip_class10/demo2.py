# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/9 20:50
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 测试商城登录验证码破解
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

from vip_class10.verify import Verify

driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)

driver.get('http://testingedu.com.cn:8000/index.php/Home/user/login.html')
driver.find_element(By.XPATH, '//*[@id="username"]').send_keys('2929184523@qq.com')
driver.find_element(By.XPATH, '//*[@id="password"]').send_keys('123456')

# 获取验证码图片
ele = driver.find_element(By.XPATH, '//*[@id="verify_code_img"]')
ele.screenshot('./verify.png')

# 调用封装好的识别方法
verify = Verify('testingzx', '1q2w3e4r.', '943050')
ver = verify.get_verify('./verify.png')
print(ver)
time.sleep(3)
driver.find_element(By.XPATH, '//*[@id="verify_code"]').send_keys(ver)
driver.find_element(By.XPATH, '//*[@id="loginform"]/div/div[6]/a').click()

time.sleep(3)
driver.quit()