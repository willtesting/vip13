# -*- coding: utf-8 -*-
"""
@Time ： 2022/8/2 22:04
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：opencv用法
@installer：pip install opencv-python -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install pyautogui
"""
import random

import cv2
import pyautogui


def FindPic(target="./big.jpg", template="./small.jpg"):
    """
    找出图像中最佳匹配位置
    :param target: 目标即背景图
    :param template: 模板即需要找到的图
    :return: 返回最佳匹配及其最差匹配和对应的坐标
    """
    target_rgb = cv2.imread(target)
    target_gray = cv2.cvtColor(target_rgb, cv2.COLOR_BGR2GRAY)
    template_rgb = cv2.imread(template, 0)
    res = cv2.matchTemplate(target_gray, template_rgb, cv2.TM_CCOEFF_NORMED)
    value = cv2.minMaxLoc(res)
    print(value)
    # 返回最佳匹配的x坐标
    return value[2][0]


def slide_by_pyautogui(x, y, offset):
    """
    使用pyautogui滑动
    :param x: 开始位置x
    :param y: 开始位置y
    :param offset: 互动横坐标偏移量
    :return:
    """
    xx = x + offset
    pyautogui.moveTo(x, y, duration=0.1)
    pyautogui.mouseDown()  # 按下鼠标键
    y += random.randint(9, 19)
    pyautogui.moveTo(x + int(offset * random.randint(15, 23) / 20), y, duration=0.28)
    y += random.randint(-9, 0)
    pyautogui.moveTo(x + int(offset * random.randint(17, 21) / 20), y,
                     duration=(random.randint(20, 31)) / 100)  # 鼠标拖动到坐标(1566,706)位置处
    y += random.randint(0, 8)
    # pyautogui.moveTo(x + int(offset * 0.1), y, duration=0.15)
    # y += random.randint(-10, 10)
    pyautogui.moveTo(xx, y, duration=0.3)
    pyautogui.mouseUp()  # 松开鼠标



if __name__ == '__main__':
    # x = FindPic()
    # print(x)

    import ctypes
    user32 = ctypes.windll.user32
    screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
    print(screensize)