# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/9 20:12
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述
import re

s = '订单号：  202212092011301252    |     付款金额（元）：  2599.00 元'

order = re.findall(r'\d{18}', s)[0]
print(order)