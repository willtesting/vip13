# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/26 20:52
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：pytest数据驱动执行类
"""
import traceback

import allure
import pytest

from vip_class16.common.Logger import logger
from vip_class16.excel_ddt.ddt import ddt


@allure.feature('#{} {}'.format(ddt.f_idx, ddt.feature_sheet))
class Test_Web:
    """pytest数据驱动执行类"""

    @allure.step
    def run_step(self, func, parmas):
        res = func(*parmas)
        return res

    @allure.story('#{} {}'.format(ddt.s_idx, ddt.suite_story))
    @pytest.mark.parametrize('cases', ddt.testsuite)
    def test_case(self, cases):
        """测试用例"""
        # 先用try，捕获用例执行失败的情况
        # logger.info(cases)
        # 取第一行，第二个单元格为用例的标题
        title = cases[0][1]
        allure.dynamic.title(title)
        logger.info(title)
        # 写入行+1
        ddt.writer.row += 1
        steps = cases[1:]
        logger.info(steps)

        # 给一个下标，记录跑了多少行
        line_index = 0
        try:
            for step in steps:
                # 获取参数
                params = step[4:7]
                # 截取不需要的参数：保留列表里面不为空的参数
                params = params[:params.index('')]
                # logger.info(params)
                # 反射，获取关键字，并调用执行
                method = step[3]
                func = getattr(ddt.obj, method)
                # 步骤
                with allure.step(step[2]):
                    res = self.run_step(func, params)

                # 对关键字结果进行处理
                if type(res) is tuple and res[0] is False:
                    logger.info(res)
                    ddt.writer.write(ddt.writer.row, 7, 'FAIL', color=2)
                    ddt.writer.write(ddt.writer.row, 8, res[1])
                    # 失败之后的步骤不写入结果
                    ddt.writer.row += len(steps) - line_index
                    pytest.fail(res[1])

                ddt.writer.write(ddt.writer.row, 7, 'PASS',color=3)
                ddt.writer.write(ddt.writer.row, 8, str(res))
                ddt.writer.row += 1
                line_index += 1

        except Exception as e:
            # 可以对执行的失败的情况针对性处理
            logger.exception(e)
            ddt.writer.write(ddt.writer.row, 7, 'FAIL',color=2)
            ddt.writer.write(ddt.writer.row, 8, str(traceback.format_exc()))
            # 失败之后的步骤不写入结果
            ddt.writer.row += len(steps) - line_index
            pytest.fail('用例执行失败')
        finally:
            if ddt.obj.driver:
                # 不管用例最终执行成功还是失败，都去截图
                allure.attach(ddt.obj.driver.get_screenshot_as_png(), '用例结果截图', allure.attachment_type.PNG)
