# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/26 20:55
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
from vip_class15.keywords.webkeys import Web
from vip_class16.common.Logger import logger,path

# 关键字对象
web = Web(path)

# 用例数据
cases = [
    [['', '', '打开浏览器', 'openbrowser', '', '', '', '', ''],
     ['', '', '进入登录页面', 'geturl', 'http://testingedu.com.cn:8000/index.php/Home/user/login.html', '', '', '', ''],
     ['', '', '输入用户名', 'input', '//*[@id="username"]', '13800138006', '', '', ''],
     ['', '', '输入密码', 'input', '//*[@id="password"]', '1234561', '', '', ''],
     ['', '', '输入验证码', 'input', '//*[@id="verify_code"]', '11111', '', '', ''],
     ['', '', '点击登录', 'click', '//*[@id="loginform"]/div/div[6]/a', '', '', '', ''],
     ['', '', '等待', 'sleep', '1', '', '', '', ''],
     ['', '', '获取提示', 'gettext', '//*[@id="layui-layer1"]/div[2]', '', '', '', ''],
     ],
    [['', '', '进入登录页面', 'geturl', 'http://testingedu.com.cn:8000/index.php/Home/user/login.html', '', '', '', ''],
     ['', '', '输入用户名', 'input', '//*[@id="username"]', '13800138006', '', '', ''],
     ['', '', '输入密码', 'input', '//*[@id="password"]', '123456', '', '', ''],
     ['', '', '输入验证码', 'input', '//*[@id="verify_code"]', '11111', '', '', ''],
     ['', '', '点击登录', 'click', '//*[@id="loginform"]/div/div[6]/a', '', '', '', ''],
     ['', '', '等待', 'sleep', '1', '', '', '', ''], ['', '', '点击登录', 'click', '//a[text()="安全退出"]', '', '', '', ''],
     ],
    [['', '', '进入登录页面', 'geturl', 'http://testingedu.com.cn:8000/index.php/Home/user/login.html', '', '', '', ''],
     ['', '', '输入用户名', 'input', '//*[@id="username"]', '13800138006', '', '', ''],
     ['', '', '输入密码', 'input', '//*[@id="password"]', '123456', '', '', ''],
     ['', '', '输入验证码', 'input', '//*[@id="verify_code"]', '11111', '', '', ''],
     ['', '', '点击登录', 'click', '//*[@id="loginform"]/div/div[6]/a', '', '', '', ''],
     ['', '', '等待', 'sleep', '1', '', '', '', ''], ],
]
