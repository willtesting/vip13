# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/26 20:52
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：pytest数据驱动执行类
"""
import pytest

from vip_class16.common.Logger import logger
from vip_class16.excel_ddt import ddt_old


class Test_Web:
    """pytest数据驱动执行类"""

    @pytest.mark.parametrize('cases',ddt_old.cases)
    def test_case(self,cases):
        """测试用例"""
        steps = cases
        for step in steps:
            if len(step[0]) > 0 or len(step[1]) > 0:
                # 分组信息，不跑
                logger.info(step)
                continue

            # 获取参数
            params = step[4:7]
            # 截取不需要的参数：保留列表里面不为空的参数
            params = params[:params.index('')]
            logger.info(params)
            # 反射，获取关键字，并调用执行
            method = step[3]
            func = getattr(ddt.web, method)
            func(*params)


