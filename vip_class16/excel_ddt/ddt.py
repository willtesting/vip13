# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/26 21:21
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import os
import time

import pytest

from vip_class15.keywords.webkeys import Web
from vip_class16.common.Excel import Reader, Writer
from vip_class16.common.Logger import path, logger
from vip_class19.appkeys import App
from vip_class21.interkeys import Inter


class DDT:
    """数据驱动，数据读取类"""

    def __init__(self):
        # 关键字对象
        self.obj: Web = None

        # 存放用例集的列表
        self.testsuite = []
        # 用来记录是第几组功能用例
        self.suite_idx = 0

        # 报告优化
        self.f_idx = 0
        self.feature_sheet = ''

        self.s_idx = 0
        self.suite_story = ''

        # writer写入执行结果
        self.writer : Writer = None

        # 用例类型
        self.case_type = 'web'


    def set_case_type(self,t='web'):
        self.case_type = t

    def run_cases(self):
        logger.warning('运行')
        # logger.info(self.testsuite)
        # 每跑一次，组数+1
        self.suite_idx += 1
        # 重命名，解决只跑第一组用例的bug
        os.rename('./excel_ddt/test_%s_%d.py' % (self.case_type,self.suite_idx - 1), './excel_ddt/test_%s_%d.py' % (self.case_type,self.suite_idx))
        pytest.main(['./excel_ddt/test_%s_%d.py' % (self.case_type,self.suite_idx), '-s', '--alluredir', './result'])

    def read_web_cases(self, case_name='电商项目用例.xlsx'):
        """把用例读成三维列表，并执行"""

        # 按用例类型，去实例关键字对象
        if self.case_type == 'web':
            self.obj = Web(path)
        elif self.case_type == 'app':
            self.obj = App()
        elif self.case_type == 'inter':
            self.obj = Inter()
        else:
            self.obj = Web(path)

        try:
            # 测试用例的列表
            test_cases = []

            reader = Reader()
            # 打开一个excel
            reader.open_excel(path + 'lib/' + case_name)

            # 打开一个结果文件
            self.writer = Writer()
            self.writer.copy_open(path + 'lib/' + case_name, path + 'lib/result-' + case_name)

            # 获取所有sheet
            sheets = reader.get_sheets()
            # 写入开始时间
            self.writer.set_sheet(sheets[0])
            self.writer.write(1,3,time.strftime( '%Y-%m-%d %H:%M:%S', time.localtime(time.time())))


            for sheet in sheets:
                # 记录sheet
                self.f_idx += 1
                self.feature_sheet = sheet
                self.s_idx = 0

                # 设置读取的sheet页面
                reader.set_sheet(sheet)
                self.writer.set_sheet(sheet)
                # 读取当前sheet的所有行
                lines = reader.readline()
                # 去掉第一行
                del lines[0]
                self.writer.row = 1
                # 遍历每一行进行如下处理
                for line in lines:
                    if len(line[0]) > 0:
                        # 这是testsuite
                        if test_cases:
                            # 如果用例步步骤列表不为空，那我们就添加到testsuite里面去
                            self.testsuite.append(test_cases)

                        if self.testsuite:
                            # 如果testsuite不为空，说明我们统计了一组用例，那就执行
                            self.run_cases()

                        # 置空用例列表，用来存下一组
                        test_cases = []

                        # 置空用例集合
                        self.testsuite = []

                        # 记录测试用例集合名字
                        self.s_idx += 1
                        self.suite_story = line[0]

                        # 行数+1
                        self.writer.row += 1

                    elif len(line[1]) > 0:
                        # 这是test_case
                        if test_cases:
                            # 如果用例步骤列表不为空，又统计到新用例开始了，那我们就添加到testsuite里面去
                            self.testsuite.append(test_cases)

                        # 置空用例列表，用来存下一组
                        test_cases = []
                        # 记录title
                        test_cases.append(line)
                    else:
                        # 这是步骤，添加到测试用例
                        test_cases.append(line)

                # 每一个sheet跑完，把最后一组用例执行
                # 这是testsuite
                if test_cases:
                    # 如果用例步步骤列表不为空，那我们就添加到testsuite里面去
                    self.testsuite.append(test_cases)

                if self.testsuite:
                    # 如果testsuite不为空，说明我们统计了一组用例，那就执行
                    self.run_cases()

                # 置空
                self.testsuite = []
                test_cases=[]

        except:
            pass
        finally:
            # 所有代码跑完，还要重命名回去
            # 重命名，解决只跑第一组用例的bug
            os.rename('./excel_ddt/test_%s_%d.py' % (self.case_type,self.suite_idx), './excel_ddt/test_%s_0.py' % self.case_type)
            # 保存Excel结果
            # 写入结束时间
            self.writer.set_sheet(sheets[0])
            self.writer.write(1,4,time.strftime( '%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
            self.writer.save_close()


# 使用全局变量，创建ddt对象，方便调用
ddt = DDT()
