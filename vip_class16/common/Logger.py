# -*- coding: utf-8 -*-
"""
@Time ： 2020/8/14 10:44
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：logging日志打印模块
"""
import logging
import os
import yaml

logger = logging.getLogger()

path = './'


# 如果你的项目名字不一样，请修改项目名字
def init(proname='vip13'):
    global logger,path
    # 获取项目绝对路径
    path = os.path.abspath(__file__)
    # 明确入口的名字进行截取
    path = path[:path.find(proname)] + proname + '\\'
    print(path)

    # file = open(xxx)
    with open(file=path + "lib/conf.yml", mode='r', encoding="utf-8") as file:
        logging_yaml = yaml.safe_load(stream=file).get('logger')
        # 处理为绝对路径
        logging_yaml['filename'] = path + logging_yaml.get('filename')

    fh = logging.FileHandler(logging_yaml.get('filename'), encoding='utf-8', mode=logging_yaml.get('filemode'))
    fh.setFormatter(logging.Formatter(logging_yaml['format']))
    # 获取根记录器：配置信息从yaml文件中获取
    logger = logging.getLogger()
    logger.setLevel(logging_yaml['level'])
    logger.addHandler(fh)

    # 创建输出到控制台的输出流
    console = logging.StreamHandler()
    # 设置日志等级
    console.setLevel(logging_yaml['level'])
    # 设置日志格式
    console.setFormatter(logging.Formatter(logging_yaml['format']))
    # 添加到logger输出
    logger.addHandler(console)


# 如果你的项目名字不一样，请修改项目名字
init()

if __name__ == "__main__":
    # 等级顺序
    logger.debug("DEBUG")
    logger.info("INFO")
    logger.warning('WARNING')
    logger.error('ERROR')
    try:
        int('a')
    except Exception as e:
        logger.exception(e)
