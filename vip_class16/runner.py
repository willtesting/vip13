# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/26 21:06
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：运行入口
"""
import os

from vip_class16.common.Logger import path,logger
from vip_class16.excel_ddt.ddt import ddt
from vip_class17.common.mail import Mail
from vip_class17.common.result import Result


# 每一次运行前，删除结果和报告（保证结果的准确性）
os.system('rd /s/q result')
os.system('rd /s/q report')


casename = 'HTTP接口用例.xlsx'
ddt.set_case_type('inter')
ddt.read_web_cases(casename)
os.system('allure generate result -o report --clean')

# 发送邮件
mail = Mail()
f = open(path + 'lib/' + mail.mail_info.get('mailmodule'), 'r', encoding='utf8')
html = f.read()
f.close()
logger.info(html)

# 结果数据获取
res = Result()
r = res.get_res(path + 'lib/result-' + casename)
print(r)
# 替换邮件模板里面的数据
for key in r:
    html = html.replace(key,r.get(key))

r = res.get_groups(path + 'lib/result-' + casename)
print(r)

# 复制模板里面tr的HTML字符串
tr = '<tr><td width="100" height="28" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc">分组名</td><td width="80" height="28" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc">用例数</td><td width="80" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc">通过数</td><td width="80" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc;color:mycolor">状态</td></tr>'
trs = ''
# 遍历行
for line in r:
    # 每一行都替换数据
    line_tr = tr.replace('分组名',line[0])
    line_tr = line_tr.replace('用例数', str(line[1]))
    line_tr = line_tr.replace('通过数', str(line[2]))
    line_tr = line_tr.replace('状态', line[3])
    if line[3] == 'PASS':
        line_tr = line_tr.replace('mycolor', 'green')
    else:
        line_tr = line_tr.replace('mycolor', 'red')

    # 把所有行加起来
    trs += line_tr

# 替换mailbody为新统计行
html = html.replace('mailbody',trs)
mail.send(html)




