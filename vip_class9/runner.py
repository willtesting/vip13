# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/7 20:36
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述
import time

from selenium.webdriver.common.by import By

from vip_class9.example import Web

web = Web()
web.open_browser()

# 登录
web.get_url('http://testingedu.com.cn:8000/Home/user/login.html')
web.input('//*[@id="username"]', '2929184523@qq.com')
web.input('//*[@id="password"]', '123456')
web.input('//*[@id="verify_code"]', '1111')
web.click('//a[@name="sbtbutton"]')

web.sleep(3)
#time.sleep(3)  # 强制等待
# 修改个人头像
web.get_url('http://testingedu.com.cn:8000/Home/User/info.html')
web.click('//*[@id="preview"]')
# 处理iframe 先进入
web.into_iframe('//*[@id="layui-layer-iframe1"]')
# 文件/图片上传时，如果标签是input，就可以使用send_keys的方法
web.input('//div[@id="filePicker"]/div[2]/input', r'/usr/local/yomi.png')  # 在win系统要注意转义D:\test\xxx
web.sleep(3)
web.click('//div[@class="saveBtn"]')
# 退出iframe
web.out_iframe()
web.click('//input[@class="save"]')
web.sleep(3)

# 新增地址
web.get_url('http://testingedu.com.cn:8000/Home/User/address_list.html')
web.click('//span[@class="co_blue"]')
web.input('//input[@name="consignee"]', 'zxtest')
web.input('//input[@name="mobile"]', '17777777777')
web.select('//*[@id="province"]', '湖南省')
web.select('//*[@id="city"]', '长沙市')
web.select('//*[@id="district"]', '25607')
web.select('//*[@id="twon"]', '25612')
web.input('//input[@name="address"]', '掌心测试地址')
web.click('//*[@id="address_submit"]')
web.sleep(2)

# 删除地址
web.click('//span[text()="zxtest"]/../..//a[text()="删除"]')
web.sleep(3)

# 搜索商品
web.input('//*[@id="q"]', '手机')
web.click('//*[@id="sourch_form"]/a')

# 获取所有商品的名字
goods = web.driver.find_elements(By.XPATH, '//div[@class="shop-list-splb p"]//div[@class="shop_name2"]/a')
for good in goods:
    print(good.text.strip())

# 添加购物车
web.click('//a[@class="num"]')
web.click('//a[contains(text(), "Huawei/华为 nova 2s")]')
web.click('//*[@id="join_cart"]')
web.sleep(3)
web.click('//span[@class="layui-layer-setwin"]/a')
web.move_to('//span[text()="我的购物车"]')
web.click('//a[@class="c-btn"]')
web.sleep(3)

# 结算
web.click('//a[text()="去结算"]')
web.sleep(1)
web.click('//button[@class="checkout-submit"]')
web.sleep(3)

# 获取订单号
t = web.driver.find_element(By.XPATH, '//p[@class="succ-p"]').text
print(t)


web.quit()