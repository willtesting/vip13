# -*- coding: utf-8 -*-
# @Author   : Mr.掌心 2929184523
# @Time     : 2022/12/7 20:02
# @Company    : 特斯汀学院 @testingedu.com.cn
# @Function  : 请输入模块功能描述
from time import sleep

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


class Web:
    def __init__(self):
        # 当前运行的浏览器
        self.br = 'gc'
        self.driver: webdriver.Chrome = None

    def open_browser(self, browser='gc'):
        """
        打开浏览器
        :param browser: 浏览器类型：支持谷歌/火狐....
        :return:
        """
        if browser == 'gc' or browser == '':
            self.br = 'gc'
            self.driver = webdriver.Chrome()
            # options = webdriver.ChromeOptions()
            # options.add_experimental_option('')
        elif browser == 'ff':
            self.br = 'ff'
            self.driver = webdriver.Firefox()
        elif browser == 'ie':
            self.br = 'ie'
            self.driver = webdriver.Ie()
        elif browser == 'eg':
            self.br = 'eg'
            self.driver = webdriver.Edge()
        else:
            self.br = 'gc'
            print('输入的浏览器不支持，默认使用谷歌浏览器...')
            self.driver = webdriver.Chrome()

        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

    def __find_element(self, locator: str = ''):
        """
        统一定位方式
        :param locator: 元素定位器，同时支持xpath/css/id
        :return: 定位到的元素，如果没有就返回None
        """
        if locator is None or locator == '':
            return None
        elif locator.startswith('/'):
            return self.driver.find_element(By.XPATH, locator)
        elif locator.startswith('#') or locator.__contains__('>'):
            return self.driver.find_element(By.CSS_SELECTOR, locator)
        else:
            return self.driver.find_element(By.ID, locator)

    def input(self, locator, value):
        """
        文本输入
        :param locator: 元素定位
        :param value: 输入的内容
        :return:
        """
        element = self.__find_element(locator)
        element.send_keys(value)

    def click(self, locator):
        """
        点击元素
        :param locator:
        :return:
        """
        element = self.__find_element(locator)
        element.click()

    def get_url(self, url):
        """打开被测地址"""
        self.driver.get(url)

    def sleep(self, t):
        """固定等待"""
        sleep(t)

    def quit(self):
        """退出"""
        self.driver.quit()

    def into_iframe(self, locator):
        """进入iframe"""
        element = self.__find_element(locator)
        self.driver.switch_to.frame(element)

    def out_iframe(self):
        """退出iframe"""
        self.driver.switch_to.default_content()

    def select(self, locator, value):
        """
        下拉框处理
        :param locator: 下拉框的定位
        :param value: 选择的值
        :return:
        """
        element = self.__find_element(locator)
        select = Select(element)
        try:
            int(value)
            select.select_by_value(value)
        except:
            select.select_by_visible_text(value)

    def move_to(self, locator):
        """鼠标悬停"""
        element = self.__find_element(locator)
        action = ActionChains(self.driver)
        # 如果不加perform，不会生效
        action.move_to_element(element).perform()







