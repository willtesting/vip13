# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 21:01
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：狗类实战
"""


# 让你写一个狗类
# 年龄，颜色，品种，大小
# 叫，摇尾巴，抓老鼠
class Dog:
    # 这一类都是中华田园犬
    dog_type = '中华田园犬'

    def __init__(self, c='黑色', w=1.0):
        #  刚出生的狗都是0岁
        self.age = 0
        self.color = c
        self.weight = w
        # 私有变量：只能在这个类里面使用
        self.__xiaoqiang = '跳蚤'

    def bark(self):
        """狗叫"""
        if self.age < 1:
            print('小狗崽呜呜叫')
        elif self.age < 3:
            print('小狗旺旺叫')
        else:
            print('大狗汪汪叫')

    def swing(self):
        """摇尾巴"""
        print('狗生下来就会摇尾巴')
        self.__yao_xq()

    @classmethod
    def get_mouse(cls):
        if cls.dog_type == '中华田园犬':
            print('中华田园犬会抓老鼠')
        else:
            print('外国狗不会抓老鼠')

    def __yao_xq(self):
        print(self.__xiaoqiang)

    @classmethod
    def set_dog_type(cls, t):
        cls.dog_type = t


if __name__ == '__main__':
    xh = Dog('黑色', 1.5)
    xh.bark()
    xh.bark()
    xh.bark()
    Dog.get_mouse()
    xh.bark()
    xh.bark()
    xh.bark()

    xh.bark()

    # xh.__yao_xq()
    xh.swing()
