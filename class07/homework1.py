# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/5 17:20
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：类的形式展现人，男人，女人
"""


class HuMan:
    """人类"""
    # 类属性
    type = '人类'

    def __init__(self,name):
        """
        构造函数，创建实例属性
        """
        # 实例变量，人的名字
        self.name = name

    def learn(self):
        """学习"""
        print('我是%s,我爱学习' % self.name)

    def work(self):
        """工作"""
        print('我爱工作，但是没钱')

    @classmethod
    def get_type(cls):
        print('我是{}'.format(cls.type))


class Man(HuMan):
    sex = '男'

    def __init__(self, name):
        """构造函数"""
        # super().__init__(name)
        HuMan.__init__(self,name)

    # 重写
    def work(self):
        print('学习为了涨工资')

    # 拓展
    @classmethod
    def get_sex(cls):
        print('我是帅锅')



if __name__ == '__main__':
    man = Man('will')
    # 继承
    man.learn()


