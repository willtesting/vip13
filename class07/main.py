# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 21:27
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：演示main入口
"""
from class07 import demo3

# 用模块实现整个项目的全局变量
demo3.my_func()
print(demo3.x)


