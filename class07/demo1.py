# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 20:30
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：狗类
"""


# 让你写一个狗类
# 年龄，颜色，品种，大小
# 叫，摇尾巴，抓老鼠
class Dog:
    """
    类的描述文档
    类名，一般会使用首字母大写
    """
    # 类变量，写在类里面、方法体之外的变量
    # 类变量尽量使用类名调用
    dog_type = '中华田园犬'

    def __init__(self, a):
        """
        构造函数：通过调用构造函数可以创建对象
        每个类都有构造方法。如果没有显式地定义构造方法，在运行的时候将会为该类提供一个默认构造方法
        构造函数是通过类名(参数)调用
        在创建一个对象的时候，如果有自己写了构造函数，则必须按写的构造函数调用。（构造函数只能有一个）
        """
        # 实例变量，在构造函数里面通过self定义的变量
        # 一般要求实例变量必须在构造函数里面显示定义
        # 因为使用实例变量一定会创建对象，一定会执行构造函数，一定会初始化这些变量，一定不会报错
        # 实例变量尽量使用实例调用
        self.name = a
        self.age = 0

    def get_age(self):
        """
        实例函数
        :return:
        """
        print(self.age)

    def set_age(self, a=0):
        """设置年龄"""
        self.age = a

    @classmethod
    def set_type(cls, t):
        """
        类函数
        cls代表类本身，调用的时候不用传
        类函数尽量使用类调用，类函数推荐操作类变量，不推荐操作实例变量
        """
        cls.dog_type = t


if __name__ == '__main__':
    # 创建对象（调用类的构造函数，创建对象）
    x_h = Dog('小黑')
    # print(x_h.__doc__)
    # # 不推荐使用示例直接创建实例变量
    # x_h.age = 0
    x_h.set_age(3)
    x_h.get_age()
    # # 这种写法是创建一个dog_type实例变量（不推荐）
    # x_h.dog_type = '哈士奇'
    # 这种写法才是对类变量重新赋值
    # Dog.dog_type = '哈士奇'
    Dog.set_type('哈士奇')

    x_b = Dog('小白')
    print(x_b.dog_type)
