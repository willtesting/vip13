# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 21:16
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：局部变量，全局变量
"""

x = 1
z = 0

if x:
    # 局部变量，原则上，它只在这个缩进范围内有效
    # 编程规范来讲，我们要求y只能在这个缩进范围内使用
    y = 0
else:
    z = 0

# 出了缩进，可能能用，因为定义过还没有释放就可以用
print(y)


def my_func():
    global x, z
    # 一个局部变量
    xx = 1
    # 我们优先认为这是局部变量
    # 只要这个模块没被销毁，都可以使用这个全局变量
    x += 1


my_func()
print(x)
