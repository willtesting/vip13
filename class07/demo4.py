# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/2 21:39
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：继承
"""
from class07.demo2 import Dog


# 拆家
class New_Dog(Dog):
    """继承"""

    def __init__(self, c='黑色', w=1.0):
        """
        如果重写了构造函数，那一定在子类的构造函数里面调用父类的构造函数
        如果你不调，那么父类的构造函数没有被执行，所以父类的实例变量都没初始化，就会报错
        记得写构造函数的参数
        """
        # Dog.__init__(self,c,w)
        super().__init__(c, w)
        # 拓展一个实例变量出来
        self.bark_times = 0

    def bark(self):
        """重写，狗叫"""
        self.bark_times += 1
        if self.age < 1:
            print('小狗崽呜呜叫')
        elif self.age < 3:
            print('小狗旺旺叫')
        else:
            print('大狗汪汪叫')

        self.age = self.bark_times // 3

    @classmethod
    def chai_jia(cls):
        if cls.dog_type == '哈士奇':
            print('二哈最擅长拆家')
        else:
            print('其他的狗子还好')




if __name__ == '__main__':
    # 继承了所有的东西
    new_dog = New_Dog()
    new_dog.bark()
    new_dog.swing()
    print(new_dog.color)
    print(New_Dog.dog_type)

    new_dog.bark()
    new_dog.bark()
    new_dog.bark()

    new_dog.bark()

    # 继承
    new_dog.get_mouse()
    # 拓展
    New_Dog.chai_jia()
    new_dog.swing()
